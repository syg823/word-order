<?php

namespace app\model\customer;

use app\model\BaseModel;
use think\model\concern\SoftDelete;
use think\model\relation\HasMany;

/**
 * 客户基本模型
 */
class CustomerBaseModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'customer_base';


    /**
     * 密码哈希加密
     * @param $value
     * @return bool|string
     */
    protected function setPasswordAttr($value)
    {
        return password_hash($value, PASSWORD_DEFAULT);
    }

    /**
     * 客户职场 一对多
     * @return HasMany
     */
    public function workplace(): HasMany
    {
        return $this->hasMany(WorkPlaceModel::class, 'customer_base_id', 'id');
    }


}