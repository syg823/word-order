<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-06-25
 * Time: 12:13
 */

namespace plugin\thinkphp\filesystem\driver;


use Iidestiny\Flysystem\Oss\OssAdapter;
use League\Flysystem\AdapterInterface;
use think\filesystem\Driver;

class Oss extends Driver
{
    protected function createAdapter(): AdapterInterface
    {
        return new OssAdapter(
            $this->config['access_key'],
            $this->config['secret_key'],
            $this->config['endpoint'],
            $this->config['bucket'],
            $this->config['isCName'],
            $this->config['root']??'',
            $this->config['buckets']??[]
        );
    }
    public function url(string $path): string
    {
        return $this->filesystem->getAdapter()->getUrl($path);
    }
}