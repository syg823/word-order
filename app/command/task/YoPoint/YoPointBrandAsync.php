<?php

namespace app\command\task\YoPoint;

use app\library\DataCollect\YoPointBrand\ConsumerOrderStruct;
use app\library\DataCollect\YoPointBrand\TerminalStruct;
use app\model\BaseModel;
use app\model\customer\WorkPlaceModel;
use app\model\device\DeviceBaseModel;
use app\model\system\SystemAreaModel;
use app\mongo\YoPoint\YoPointBrandConsumerOrderMongo;
use app\mongo\YoPoint\YoPointBrandOperatorMongo;
use app\mongo\YoPoint\YoPointBrandTerminalMongo;
use app\service\DataCollect\YoPoint\YoPointBrandService;
use app\service\system\AreaService;
use Exception;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\PhpProcess;
use Symfony\Component\Process\Process;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Log;

class YoPointBrandAsync extends Command
{
    private YoPointBrandService $yoPointService;

    protected function configure()
    {
        // 指令配置
        $this->setName('同步有朋')
            ->setDescription('同步有朋数据');
        $this->yoPointService = new YoPointBrandService();
    }

    protected function execute(Input $input, Output $output)
    {
        //$this->test();
    }


    /**
     * 售货机订单列表
     * @return void
     */
    private function consumerOrderList()
    {
        $firstPage = 1;
        $startTime = time() - (600);
        $endTime = time();
        $consumerOrderListRes = $this->yoPointService->consumerOrderList($firstPage,
            $startTime, $endTime);
        if ($consumerOrderListRes->code !== 0) {
            $this->output->writeln($consumerOrderListRes->msg);
            return;
        }
        $dataList = $consumerOrderListRes->data['data'];
        $lastPage = $consumerOrderListRes->data['last_page'];
        $currentPage = $consumerOrderListRes->data['current_page'];
        for ($i = $currentPage + 1; $i <= $lastPage; $i++) {
            $consumerOrderListRes = $this->yoPointService->consumerOrderList($i, $startTime, $endTime);
            if ($consumerOrderListRes->code !== 0) {
                $this->output->writeln($consumerOrderListRes->msg);
                return;
            }
            $dataList = array_merge($dataList, $consumerOrderListRes->data['data']);
        }

        if (!empty($dataList)) {
            $mongo = new YoPointBrandConsumerOrderMongo();
            foreach ($dataList as $consumerOrderStruct) {
                /**
                 * @var ConsumerOrderStruct $consumerOrderStruct
                 */
                $data = $consumerOrderStruct->toArray();

                // mongo 处理
                $isEmpty = $mongo->where(['id' => $consumerOrderStruct->id])->findOrEmpty()->isEmpty();
                if ($isEmpty) {
                    echo "Mongo不存在数据,进行保存" . PHP_EOL;
                    $mongo->insertOne($data);
                } else {
                    echo "Mongo存在数据, 更新数据" . PHP_EOL;
                    $mongo->updateByWehere($data, ['id' => $consumerOrderStruct->id]);
                }

                // echo $consumerOrderStruct->ProductName. PHP_EOL;

            }
        }
    }





    private function test()
    {
        $processFun = function (){
            $process = Process::fromShellCommandline('ls -a');
            $process->start();

//            echo $process->getOutput();
//            $process->wait();
        };
        $processFun();
    }


}