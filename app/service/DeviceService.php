<?php

namespace app\service;

use app\library\DataReturn;
use app\model\device\DeviceBaseModel;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 设备服务
 */
class DeviceService
{
    private DeviceBaseModel $model;

    public function __construct()
    {
        $this->model = new DeviceBaseModel();
    }

    /**
     * 获取设备列表
     * @param numeric $customerID
     * @return DataReturn
     */
    public function getDeviceList($customerID = 0): DataReturn
    {
        // 先获取职场
        $serviceWorkPlace = new WorkPlaceService();
        $workPlaceUUIDName = $serviceWorkPlace->getWorkPlaceUUIDNameList($customerID)->data;
        // 设备列表
        $deviceList = [];
        if (!empty($workPlaceUUIDName)) {
            $workPlaceUuid = array_keys($workPlaceUUIDName);
            try {
                $deviceList = $this->model->whereIn('work_place_uuid', $workPlaceUuid)
                    ->order('id desc')
                    ->select()->toArray();
            } catch (DataNotFoundException|ModelNotFoundException|DbException $e) {
                return dataErrorReturn($e->getMessage());
            }
        }
        return dataReturn($deviceList);
    }


}