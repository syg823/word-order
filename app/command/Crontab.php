<?php

namespace app\command;

use Fairy\HttpCrontab;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;

/**
 * 计划任务 php think crontab start
 * http://127.0.0.1:2350
 */
class Crontab extends Command
{
    protected function configure()
    {
        $this->setName('定时任务')->addArgument('option', Argument::OPTIONAL, "your option");
    }

    protected function execute(Input $input, Output $output)
    {
        //date_default_timezone_set('PRC');
        date_default_timezone_set('Asia/Shanghai');

        $dbConfig = [
            'hostname' => env('database.hostname', '127.0.0.1'),
            'hostport' => env('database.hostport', '3306'),
            'username' => env('database.username', 'superenergy'),
            'password' => env('database.password', 'superenergy'),
            'database' => env('database.database', 'superenergy'),
            'charset' => env('database.charset', 'utf8mb4')
        ];
        $isDebug = env('ENV') != 'release';

        $logo = <<<EOL
╭╮　╭☆╭──╮╭╮   ☆╮   ╭───╮
││　│││╭─★││   ││   ★╭─╮│
│╰─╯││╰╮　││   ││   ││ ││
│╭─╮││╭╯　││   ││   ││ ││
││　│││╰─╮│╰──╮│╰──╮│╰─╯│
╰★　╰╯╰──╯☆───╯★───╯╰───☆
EOL;

        $output->writeln($logo . PHP_EOL);
        $isDebug && $output->writeln("Debug Mode" . PHP_EOL);
        $socketUrlPort = env('CRONTAB.crontab_base_uri');

        $httpCrontab = new HttpCrontab($socketUrlPort);
        if ($isDebug) {
            $httpCrontab->setDebug();
        }
        $httpCrontab
            ->setName('Http Crontab Server')
            ->setDbConfig($dbConfig)
            ->run();
    }
}