<?php

namespace app\command\test;

use app\model\customer\WorkPlaceModel;
use app\model\device\DeviceBaseModel;
use app\model\system\SystemAreaModel;
use app\service\CrontabService;
use app\service\DataCollect\YoPoint\YoPointBrandService;
use app\service\statistics\device\CountDrinkService;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Db;

class TestAnything extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('testanything')
            ->setDescription('the test anything command');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->testCrontab();
    }

    public function testYp()
    {
        $service = new YoPointBrandService();
//        $method = "api.test";
//        $service->requestApi($method);
//        $res = $service->terminalTerminalList();
//        var_export($res->data);
//        foreach ($res->data['data'] as $datum) {
//            var_export($datum->toArray());
//            break;
//        }
//        $res = $service->consumerOrderList(1, time()-(3600*3), time());
//        var_export($res->data);
//        $res = $service->terminalProductJournalList('6463922aae6b90001170cceb',1,10, '2024-04-06 - 2024-04-12');
//        var_export($res->data);


        $months = getMonthsRangeFromTimestamp(time(), 12);
        var_export($months);
    }

    public function testCountDrink()
    {
        $service = new CountDrinkService();
        $deviceUUID = ['6463922bae6b90001170cd52','6463922bae6b90001170cd2f'];
        $res= $service->dayTotalGroupDevice($deviceUUID, date('Y-m-d', strtotime('-1 day')));
        var_export($res);
    }

    public function testArea()
    {

        $modelWorkPlace = new WorkPlaceModel();
        $res =Db::table($modelWorkPlace->getTable())->distinct()->field('province_id,city_id,district_id')->column('province_id,city_id,district_id');
        var_export($res);
    }

    public function testCrontab()
    {
        $service = new CrontabService();
        $res = $service->CrontabList();
        var_export($res);
    }

}