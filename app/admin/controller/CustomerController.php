<?php

namespace app\admin\controller;

use app\admin\controller\AdminBaseController;
use app\model\customer\CustomerBaseModel;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Actions;
use ExAdmin\ui\component\grid\grid\Grid;
use plugin\thinkphp\Admin;
use think\Model;

class CustomerController extends AdminBaseController
{

    /**
     * @inheritDoc
     */
    protected function modelSet(): Model
    {
        return new CustomerBaseModel();
    }


    /**
     * @return Grid
     */
    public function index(): Grid
    {
        $callFunction = function (Grid $grid) {
            $this->hideGridItems($grid);
            $grid->model()->order(["id" => 'desc']);
            $grid->title("客户");
            $grid->column("name", "客户全称")->width(300);
            $grid->column('create_time', '创建时间');

            $grid->deling(function ($ids) {
                // todo 删除逻辑
                return message_error('失败');
            });

            $grid->setForm()->drawer($this->form())->width("50%");
            $grid->actions(function (Actions $actions, $data) {
                $actions->dropdown()
                    ->prepend(admin_trans('admin.reset_password'), 'fas fa-key')
                    ->modal($this->resetPassword($data['id']));

            });

        };
        return Grid::create($this->model, $callFunction);
    }

    /**
     * @return Form
     */
    public function form(): Form
    {
        $callFunction = function (Form $form) {
            $title = $form->isEdit() ? "编辑文章" : "新增文章";
            $form->title($title);
            $form->hidden("admin_id")->value(Admin::id());

            // 第一行
            $form->row(function (Form $form) {
                // 第一列
                $form->column(function (Form $form) {

                    $form->textarea("name", "客户全称")->rows(3)->required();
                    $form->textarea("username", "用户名")->rows(3)->required();
                    if ( !$form->isEdit() ){
                        $form->password("password", "登录密码")
                            ->allowClear(true)
                            ->default(123456)
                            ->help('初始化密码123456,建议密码包含大小写字母、数字、符号')
                            ->required();
                        $form->hidden("uuid")->value(uuidByComb());
                    }
                })->span(12);

            });
        };
        return Form::create($this->model, $callFunction);
    }

    /**
     * 重置密码
     * @auth true
     * @param $id
     * @return Form
     */
    public function resetPassword($id)
    {
        return Form::create($this->model,function (Form $form){
            $form->password('password', admin_trans('admin.new_password'))
                ->rule([
                    'confirm' => admin_trans('admin.password_confim_validate'),
                    'min:6' => admin_trans('admin.password_min_number')
                ])
                ->value('')
                ->required();
            $form->password('password_confirm', admin_trans('admin.confim_password'))
                ->required();
        });
    }


}