<?php
return [
    'title'=>'Attachment management',
    'download'=>'Download',
    'cate'=>[
        'fields'=>[
            'name'=>'Category Name',
            'pid'=>'Category parent',
            'permission_type'=>'Permission type',
            'sort'=>'Sort',
        ],
        'parent'=>'Top level classification',
        'public'=>'Public',
        'private'=>'Private',
    ]
];
