<?php

namespace plugin\thinkphp\model;

use think\Model;

class AdminRole extends Model
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.role_table');
        parent::__construct($data);
    }

    /**
     * 部门
     * @return \think\model\relation\BelongsToMany
     */
    public function department(){
        return $this->belongsToMany(plugin()->thinkphp->config('database.department_model'),plugin()->thinkphp->config('database.role_department_table'), 'department_id', 'role_id');
    }
}
