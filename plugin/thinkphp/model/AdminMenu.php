<?php

namespace plugin\thinkphp\model;

use think\Model;

class AdminMenu extends Model
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.menu_table');
        parent::__construct($data);
    }

    protected function getNameAttr($value)
    {
        return admin_trans('menu.titles.' . $value, $value);
    }
}
