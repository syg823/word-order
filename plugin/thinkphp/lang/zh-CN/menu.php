<?php
return [
    'add'=>'添加菜单',
    'title'=>'系统菜单管理',
    'fields'=>[
        'top'=>'顶级菜单',
        'pid'=>'上级菜单',
        'name'=>'菜单名称',
        'url'=>'菜单链接',
        'icon'=>'菜单图标',
        'sort'=>'排序',
        'status'=>'状态',
        'open'=>'菜单展开',
        'super_status'=>'超级管理员状态',
    ],
    'options'=>[
        'admin_visible'=>[
            [1 => '显示'],
            [0 => '隐藏']
        ]
    ],
    'titles'=>[
        'home' => '首页',
        'system' => '系统',
        'system_manage' => '系统管理',
        'config_manage' => '配置管理',
        'attachment_manage' => '附件管理',
        'permissions_manage' => '权限管理',
        'admin' => '用户管理',
        'role_manage' => '角色管理',
        'menu_manage' => '菜单管理',
        'plug_manage' => '插件管理',
        'department_manage' => '部门管理',
        'post_manage' => '岗位管理',
    ]
];
