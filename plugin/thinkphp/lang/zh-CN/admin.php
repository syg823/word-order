<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-15
 * Time: 23:39
 */
return [
    'user_info' => '个人信息',
    'system_user'=>'系统用户',
    'not_access_permission' => '没有访问该操作的权限',
    'super_admin'=>'超级管理员',
    'super_admin_delete'=>'超级管理员不可以删除!',
    'super_admin_disabled'=>'超级管理员不可以禁用!',
    'reset_password'=>'重置密码',
    'old_password'=>'旧密码',
    'old_password_error'=>'旧密码错误',
    'new_password'=>'新密码',
    'confim_password'=>'确认密码',
    'access_rights'=>'访问权限',
    'normal'=>'正常',
    'disable'=>'禁用',
    'username_exist'=>'用户名存在重复',
    'phone_exist' => '手机号已存在',
    'password_min_number'=>'密码最少6位数',
    'password_confim_validate'=>'输入密码不一致',
    'update_password'=>'修改密码',
    'fields'=>[
        'username'=>'用户名' ,
        'nickname'=>'用户昵称' ,
        'avatar'=>'用户头像'  ,
        'password'=>'密码'  ,
        'phone'=>'手机号'  ,
        'mail'=>'邮箱'  ,
        'status'=>'账号状态'  ,
        'create_at'=>'创建时间',
    ],
];
