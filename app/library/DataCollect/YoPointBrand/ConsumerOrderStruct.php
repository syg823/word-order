<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

class ConsumerOrderStruct extends StructFactory
{
    public string $UserID;
    public string $TID;
    public string $OID;
    public string $BID;
    public string $BarCode;
    public float $OriginalPrice;
    public float $Price;
    public float $Discount;
    public float $CostPrice;
    public int $Temperature;
    public int $PayType;
    public string $ClientIPAddress;
    public array $PayExtend;
    public int $ScenesType;
    public int $Platform;
    public string $ReceiptNo;
    public string $FID;
    public string $ReplenisherAccountID;
    public string $ExplainLocation;
    public string $Address;
    public string $PlaceID;
    public string $InnerCode;
    public string $PlaceName;
    public string $Country;
    public string $Province;
    public string $City;
    public string $District;
    public string $LandMarkName;
    public string $IndustryParentName;
    public string $IndustryName;
    public string $TerminalName;
    public string $DeviceCode;
    public string $DeviceProductionCode;
    public string $TradeNo;
    public int $PayTime;
    public int $TradeStatus;
    public int $RefundsTime;
    public int $RefundsType;
    public float $BrandSellSupplyPrice;
    public string $CabinetName;
    public int $CargoNum;
    public string $CargoDisplayName;
    public int $MotorIndex;
    public int $ShipmentsTime;
    public int $ShipmentsStatus;
    public string $ShipmentsError;
    public int $UserRefundsStatus;
    public int $UserRefundsTime;
    public string $UserRefundsReason;
    public string $UserRefundsRemarks;
    public array $UserRefundsImageUrl;
    public string $OpAccountID;
    public string $RefundsRemarks;
    public string $FromReceiptNo;
    public int $BizSource;
    public array $FromOrderInfo;
    public int $UserTakeTime;
    public int $CreateAt;
    public int $UpdatedAt;
    public string $id;
    public UserVOStruct $UserVO;
    public ProductVOStruct $ProductVO;
    public TerminalVOStruct $TerminalVO;
    public string $ProductName;
}