<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateAdminOrganization extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('admin_department', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('部门表');
        $table->addColumn(Column::integer('pid')->setDefault(0)->setNullable()->setComment('上级部门'));
        $table->addColumn(Column::string('name',100)->setComment('上级部门'));
        $table->addColumn(Column::string('leader',100)->setNullable()->setComment('负责人'));
        $table->addColumn(Column::string('phone',100)->setNullable()->setComment('手机号'));
        $table->addColumn(Column::boolean('status')->setDefault(1)->setComment('状态：1=正常,0=禁用'));
        $table->addColumn(Column::integer('sort')->setDefault(0)->setComment('排序'));
        $table->addColumn(Column::string('path')->setNullable());
        $table->addSoftDelete();
        $table->addTimestamps();
        $table->create();

        $table = $this->table('admin_post', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('岗位表');
        $table->addColumn(Column::string('name',100)->setComment('岗位名称'));
        $table->addColumn(Column::boolean('status')->setDefault(1)->setComment('状态：1=正常,0=禁用'));
        $table->addColumn(Column::integer('sort')->setDefault(0)->setComment('排序'));
        $table->addSoftDelete();
        $table->addTimestamps();
        $table->create();

        $table = $this->table('admin_role_department', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('角色数据权限部门关联表');
        $table->addColumn(Column::integer('role_id')->setDefault(0)->setComment('角色id'));
        $table->addColumn(Column::integer('department_id')->setDefault(0)->setComment('部门id'));
        $table->create();

        $table = $this->table('admin_roles', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统角色表');
        $table->addColumn(Column::boolean('check_strictly')->setDefault(0));
        $table->addColumn(Column::tinyInteger('data_type')->setDefault(0))
            ->setComment('数据权限类型:0=全部数据权限,1=自定义数据权限,2=本部门及以下数据权限,3=本部门数据权限,4=本人数据权限');
        $table->update();

        $table = $this->table('admin_users', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统用户表');
        $table->addColumn(Column::integer('department_id')->setNullable()->setComment('部门id'));
        $table->addColumn(Column::json('post')->setNullable()->setComment('岗位'));
        $table->update();

    }
}
