<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

class TerminalVOStruct extends StructFactory
{
    public string $Name;
    public string $DeviceCode;
    public string $Code;
}