<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

/**
 * 有朋品牌商服务下的运营商
 */
class OperatorStruct extends StructFactory
{
    /** @var string 运营商ID */
    public string $OID;

    /** @var string 运营商名称 */
    public string $Name;

    /**
     * @var string 运营商别名
     */
    public string $AliasName;

    /** @var string 省份 */
    public string $Province;

    /** @var string 城市 */
    public string $City;

    /** @var string 区域 */
    public string $District;

    /** @var numeric 电话号码 */
    public $PrincipalPhone;

    /** @var int 自取柜总数 */
    public int $CabinetTotal;

    /** @var int 售货机总数 */
    public int $TermianlTotal;

    /** @var int 创建时间 */
    public int $CreateAt;
}