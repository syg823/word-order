<?php

namespace app\command\task\YoPoint;

use app\model\BaseModel;
use app\model\customer\WorkPlaceModel;
use app\model\system\SystemAreaModel;
use app\mongo\YoPoint\YoPointBrandOperatorMongo;
use app\service\DataCollect\YoPoint\YoPointBrandService;
use app\service\system\AreaService;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Log;

/**
 * 同步友朋运营商列表到职场
 */
class YoPointBrandAsyncOperators extends Command
{
    private YoPointBrandService $yoPointService;

    protected function configure()
    {
        // 指令配置
        $this->setName('同步友朋运营商列表到职场')
            ->setDescription('同步友朋运营商列表到职场');
        $this->yoPointService = new YoPointBrandService();
    }

    protected function execute(Input $input, Output $output)
    {
        $this->operatorsList();
    }

    /**
     * 运营商即职场同步
     * @return void
     */
    private function operatorsList()
    {
        $pageSize = 20;
        $firstPage = 1;
        $operatorListRes = $this->yoPointService->operatorsList($firstPage, $pageSize);
        $dataList = $operatorListRes->data['data'];
        $lastPage = $operatorListRes->data['last_page'];
        $currentPage = $operatorListRes->data['current_page'];
        for ($i = $currentPage + 1; $i <= $lastPage; $i++) {
            $operatorListRes = $this->yoPointService->operatorsList($i, $pageSize);
            $dataList = array_merge($dataList, $operatorListRes->data['data']);
        }

        if (!empty($dataList)) {
            $mongo = new YoPointBrandOperatorMongo();
            $model = new WorkPlaceModel();
            $areaService = new AreaService();
            foreach ($dataList as $value) {
                $data = $value->toArray();
                $data['uuid'] = $data['OID'];
                $province_id = $areaService->getIdByName($data['Province'], SystemAreaModel::levelProvince);
                $city_id = $areaService->getIdByName($data['City'], SystemAreaModel::levelCity, $province_id);
                $district_id = $areaService->getIdByName($data['District'], SystemAreaModel::levelDistrict, $city_id);

                if ($province_id == 0 || $city_id == 0 || $district_id == 0) {
                    $error = $data['OID'] . "存在地址错误 $province_id:" . $data['Province'] . " - $city_id:" . $data['City'] . " - $district_id:" . $data['District'];
                    Log::write($error);
                    $this->output->writeln($error);
                    continue;
                }

                $data['province_id'] = $province_id;
                $data['city_id'] = $city_id;
                $data['district_id'] = $district_id;
                $data['name'] = $data['Name'];
                $data['alias_name'] = $data['AliasName'];

                $modelData = [
                    'uuid' => $data['uuid'],
                    'name' => $data['name'],
                    'alias_name' => $data['alias_name'],
                    'province_id' => $province_id,
                    'city_id' => $city_id,
                    'district_id' => $district_id,
                    'data_source_from' => BaseModel::dataSourceFromYoPoint,
                ];

                //var_export($data);

                // mongo 处理
                $isEmpty = $mongo->where(['OID' => $data['OID']])->findOrEmpty()->isEmpty();
                if ($isEmpty) {
                    echo "Mongo不存在数据,进行保存" . PHP_EOL;
                    $mongo->insertOne($data);
                } else {
                    echo "Mongo存在数据, 更新数据" . PHP_EOL;
                    $mongo->updateByWehere($data, ['OID' => $data['OID']]);
                }
                // model 处理
                $isEmpty = $model->where(['uuid' => $modelData['uuid']])->findOrEmpty()->isEmpty();
                if ($isEmpty) {
                    echo "Model不存在数据,进行保存" . PHP_EOL;
                    $model->insertOne($modelData);
                } else {
                    echo "Model存在数据, 更新数据" . PHP_EOL;
                    $model->updateByWehere($modelData, ['uuid' => $modelData['uuid']]);
                }
            }
        }

    }


}