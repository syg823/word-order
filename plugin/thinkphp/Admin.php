<?php

namespace plugin\thinkphp;


use ExAdmin\ui\component\form\field\Editor;
use ExAdmin\ui\component\form\field\upload\File;
use ExAdmin\ui\component\form\field\upload\Image;
use ExAdmin\ui\support\Container;
use ExAdmin\ui\support\Token;
use plugin\thinkphp\controller\AttachmentController;
use think\facade\Cache;
use think\facade\Filesystem;
use think\facade\Route;

class Admin
{
    protected static $permissions = [];
    /**
     * 方法是否存在
     * @param string $class 类
     * @param string $method 方法
     * @return bool
     * @throws \ReflectionException
     */
    public static function methodExists($class, $method)
    {
        $constructor = new \ReflectionClass($class);
        if ($constructor->hasMethod($method)) {
            $method = $constructor->getMethod($method);
            if ($constructor->name == $method->class) {
                return true;
            }
        }
        return false;
    }

    /**
     * 权限节点
     * @return \ExAdmin\ui\auth\Node
     */
    public static function node()
    {
        return \ExAdmin\ui\support\Container::getInstance()->node;
    }

    /**
     * 权限
     * @return array
     */
    public static function permission()
    {
        if(!isset(self::$permissions[Admin::id()])){
            self::$permissions[Admin::id()] = Admin::user()->permission->column('node_id');
        }
        $permission = self::$permissions[Admin::id()];
        return $permission;
    }
    /**
     * 角色
     * @return array
     */
    public static function role()
    {
        return Admin::user()->roles->column('id');
    }
    /**
     * 用户
     * @return mixed
     */
    public static function user()
    {
        return Token::user();
    }

    /**
     * 用户id
     * @return int
     */
    public static function id()
    {
        return Token::id();
    }

    public static function check($class, $function, $method)
    {
        $node = Admin::node()->all();
        $node = array_column($node, 'id');
        $actions[] = str_replace('-', '\\', $class) . '\\' . $function;
        $actions[] = str_replace('-', '\\', $class) . '\\' . $function. '-' . strtolower($method);
        foreach ($actions as $action){
            if (in_array($action, $node)){
                if (Admin::id() == plugin()->thinkphp->config('admin_auth_id')) {
                    return true;
                }
                if (!in_array($action, Admin::permission())) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function getDispatch()
    {

        $controller = request()->controller();
        $class = null;
        $function = null;
        if ($controller) {
            $class = app()->getNamespace().'\\controller\\'.request()->controller();
            $function = request()->action();
        } elseif (strpos(request()->pathinfo(),'ex-admin')===0) {
            $params = app()->route->check()->getParam();
            $class = $params['class'];
            $function = $params['function'];
        }
        return [$class, $function];
    }

    /**
     * 上传初始化配置
     */
    public static function uploadInit()
    {
        $uploadDiskConfig = function ($disk) {
            $config = config("filesystem.disks.$disk");
            //上传初始化
            $uploadConfig['driver'] = $config['type'];
            $adapter = app('filesystem')->disk($config['type'])->getAdapter();
            if ($config['type'] == 'qiniu') {
                $uploadConfig['domain'] = $config['domain'];
                $uploadConfig['uploadToken'] = $adapter->getUploadToken(null, 3600 * 3);
            } elseif ($config['type'] == 'oss') {
                $adapter->setCdnUrl($config['domain']);
                $uploadConfig['domain'] = $config['domain'];
                $uploadConfig['accessKey'] = $config['access_key'];
                $uploadConfig['secretKey'] = $config['secret_key'];
                $uploadConfig['region'] = $config['region'];
                $uploadConfig['bucket'] = $config['bucket'];
            }
            return $uploadConfig;

        };
        $uploadDisk = function ($disk) use ($uploadDiskConfig) {
            $uploadConfig = $uploadDiskConfig($disk);
            foreach ($uploadConfig as $key => $value) {
                $this->$key($value);
            }
            $this->attr('disk', $disk);
            return $this;
        };
        Image::addMethod('disk', $uploadDisk);
        File::addMethod('disk', $uploadDisk);
        Editor::addMethod('disk', function ($disk) use ($uploadDiskConfig) {
            $uploadConfig = $uploadDiskConfig($disk);
            $uploadConfig['disk'] = $disk;
            $this->upload($uploadConfig + ['progress' => true]);
        });

        $finder = function ($upload, $type = '') {
            $grid = Container::getInstance()
                ->make(\ExAdmin\ui\Route::class)
                ->invokeMethod(AttachmentController::class, 'index', [
                        'size' => $upload->attr('fileSize'),
                        'ext' => $upload->attr('ext'),
                        'type' => $type,
                        'customStyle' => null
                    ]
                );
            $grid->selectionField('url');
            $grid->params(['selectionField'=>'url']);
            $attrs = $upload->getAttrs();
            unset($attrs['progress'], $attrs['onlyShow'], $attrs['type']);
            $grid->attr('tools')[0]->attrs($attrs);
            $upload->attr('finder', $grid);
        };
        Image::beforeEnd(function ($image) use ($finder) {
            $finder($image, 'image');
        });
        File::beforeEnd(function ($file) use ($finder) {
            $finder($file);
        });
    }
}
