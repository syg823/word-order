<?php

namespace app\model\device;

use app\model\BaseModel;
use think\model\concern\SoftDelete;
use think\model\relation\BelongsTo;

/**
 * 设备型号(设备模板库)
 */
class DeviceClassModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'device_class';


    /**
     * @return BelongsTo
     */
    public function deviceType(): BelongsTo
    {
        return $this->belongsTo(DeviceTypeModel::class, 'device_type_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function deviceBrand(): BelongsTo
    {
        return $this->belongsTo(DeviceBrandModel::class, 'device_brand_id', 'id');
    }


}
