<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-10
 * Time: 22:12
 */

namespace plugin\thinkphp\model;
use think\Model;
use think\model\concern\SoftDelete;

class AdminUser extends Model
{
    use  SoftDelete;

    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.user_table');
        parent::__construct($data);
    }

    /**
     * 部门
     * @return \think\model\relation\BelongsTo
     */
    public function department(){
        return $this->belongsTo(plugin()->thinkphp->config('database.department_model'), 'department_id');
    }
    /**
     * 角色
     * @return \think\model\relation\BelongsToMany
     */
    public function roles(){
        return $this->belongsToMany(plugin()->thinkphp->config('database.role_model'),plugin()->thinkphp->config('database.role_user_model'),'role_id','user_id');
    }

    /**
     * 功能
     * @return \think\model\relation\HasManyThrough
     */
    public function permission()
    {
        return $this->hasManyThrough(plugin()->thinkphp->config('database.role_permission_model'), plugin()->thinkphp->config('database.role_user_model'), 'user_id', 'role_id','id','role_id');
    }

    /**
     * 密码哈希加密
     * @param $value
     * @return bool|string
     */
    protected function setPasswordAttr($value){
        return password_hash($value,PASSWORD_DEFAULT);
    }

}
