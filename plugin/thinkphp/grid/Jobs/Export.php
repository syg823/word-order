<?php

namespace plugin\thinkphp\grid\Jobs;

use ExAdmin\ui\Route;
use ExAdmin\ui\support\Container;
use think\facade\Request;
use think\queue\Job;
class Export
{

    public function fire(Job $job, $data)
    {
        $data['ex_admin_queue'] = false;
        $class = str_replace('-', '\\', $data['ex_admin_class']);
        $_REQUEST = $data;

        Request::withInput($data);
        Container::getInstance()
            ->make(Route::class)
            ->invokeMethod($class, $data['ex_admin_function'], $data)
            ->jsonSerialize();
    }
}
