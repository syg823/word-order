<?php

namespace app\mongo\statistics\device;

use app\mongo\MongoBase;

/**
 * @method multiAggregate(string[] $array, string[] $array1)
 */
class CountDeviceDaySkuMongo extends MongoBase
{
    protected $table = 'CountDeviceDaySkuMongo';
    protected $mapping = [];
}