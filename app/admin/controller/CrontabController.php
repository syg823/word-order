<?php

namespace app\admin\controller;

use app\model\system\CrontabModel;
use app\service\CrontabService;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Actions;
use ExAdmin\ui\component\grid\grid\Grid;
use think\Model;

class CrontabController extends AdminBaseController
{

    /**
     * @inheritDoc
     */
    protected function modelSet(): Model
    {
        return new CrontabModel();
    }

    /**
     * 列表
     * @return Grid
     */
    public function index(): Grid
    {
        $callFunction = function (Grid $grid) {
            $this->hideGridItems($grid);
            $grid->model()->order(["create_time" => 'desc']);
            $grid->setForm()->modal($this->form());

            $grid->column("title", "任务类型");
            $grid->column("type", "任务类型")->using(CrontabModel::typeOptions);
            $grid->column("frequency", "任务频率");
            $grid->column("running_times", "已执行");
            $grid->column("last_running_time", "上次执行")->display(function ($val) {
                return date("Y-m-d H:i:s", $val);
            });
            $grid->column("shell", "执行脚本");
            $grid->column("remark", "任务备注");
            $grid->column("status", "状态")->using([1 => '启用', 0 => '禁用']);
            $grid->column("sort", "排序");
            // 操作列按钮
            $grid->actions(function (Actions $action, $data) {
                // 隐藏删除
                $action->hideDel();
            });
        };
        return Grid::create($this->model, $callFunction);
    }

    /**
     * 添加修改
     * @return Form
     */
    public function form(): Form
    {
        $task_id = request()->post('id');
        $taskData = $this->model->getInfoById($task_id)->data;
        if (!is_null($taskData)) {
            $taskData = $taskData->toArray();
        } else {
            $taskData = [];
        }
        return Form::create($taskData, function (Form $form) use ($task_id) {
            $form->title("计划任务");
            $form->text('title', "任务标题")->required();
            $form->select('type', "任务类型")
                ->required()
                ->options(CrontabModel::typeOptions);
            $form->text("frequency", "任务频率")->required();
            $form->textarea("shell", "执行脚本")->required();
            $form->textarea("remark", "任务备注");
            $form->switch("status", '状态')
                ->checkedValue(1)
                ->unCheckedValue(0)
                ->default(1)
                ->span(6)
                ->required();
            $form->number("sort", "排序")->default(0)
                ->min(0);

            $form->saving(function (Form $form) use ($task_id) {
                $param = $form->input();
                $crontabService = new CrontabService();
                if ($form->isEdit()) {
                    // 更新
                    $crontabService->edit($param);
                    // 状态
                    $modifySingleProperty = [
                        'id' => $task_id,
                        'field' => 'status',
                        'value' => $param['status'],
                    ];
                    $crontabService->modify($modifySingleProperty);
                } else {
                    $crontabService->add($param);
                }

            });

        });
    }


}