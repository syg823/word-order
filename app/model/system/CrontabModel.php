<?php

namespace app\model\system;

use app\model\BaseModel;

/**
 * 计划任务模型
 */
class CrontabModel extends BaseModel
{
    protected $table = 'crontab_task';

    const typeOptions = [
        0 => '请求url',
        1 => '执行sql',
        2 => '执行shell'
    ];

}