<?php

namespace ExAdmin\ui\component\echart {

    use think\Model;

    /**
     * @method $this count(Model $model, $text,\Closure $query = null, $dateField = 'create_time') 统计数量
     * @method $this max(Model $model, $text, $field, \Closure $query = null, $dateField = 'create_time') 统计最大值
     * @method $this avg(Model $model, $text, $field, \Closure $query = null, $dateField = 'create_time') 统计平均值
     * @method $this sum(Model $model, $text, $field, \Closure $query = null, $dateField = 'create_time') 统计总和
     * @method $this min(Model $model, $text, $field, \Closure $query = null, $dateField = 'create_time') 统计最小值
     */
    class Echart
    {

    }
}
