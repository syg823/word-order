<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-14
 * Time: 21:03
 */

namespace plugin\thinkphp\model;



use think\model\Pivot;

class AdminRoleUsers extends Pivot
{

    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.role_user_table');
        parent::__construct($data);
    }
    public function a(){
        return $this->belongsTo(AdminUser::class,'user_id');
    }
}
