<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

/**
 * 场地策略
 */
class PlaceVOStruct extends StructFactory
{
    /**
     * @var string 场地策略-名称
     */
    public string $Name;

    /**
     * @var string 场地策略-省
     */
    public string $Province;

    /**
     * @var string 场地策略-市
     */
    public string $City;

    /**
     * @var string 场地策略-区
     */
    public string $District;

    /**
     * @var string 场地策略-主体
     */
    public string $Subject;

    /**
     * @var string 场地策略-商圈名称
     */
    public string $LandMarkName;

    /**
     * @var string 场地策略-行业分类父级
     */
    public string $IndustryParentName;

    /**
     * @var string 场地策略-行业分类
     */
    public string $IndustryName;
}