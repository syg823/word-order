<?php

namespace plugin\thinkphp\controller;

use plugin\thinkphp\Admin;
use ExAdmin\ui\component\common\Html;
use ExAdmin\ui\component\common\Icon;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Grid;
use ExAdmin\ui\support\Arr;


/**
 * 菜单管理
 */
class MenuController
{
    protected $model;

    public function __construct()
    {
        $this->model = plugin()->thinkphp->config('database.menu_model');
    }
    /**
     * 系统菜单
     * @auth true
     * @return Grid
     */
    public function index():Grid
    {
        return Grid::create(new $this->model(),function (Grid $grid){
            $grid->title(admin_trans('menu.title'));
            $grid->model()->order('sort');
            $grid->tree();
            $grid->column('name', admin_trans('menu.fields.name'))->display(function ($value, $data) {
                return Html::create([
                    Icon::create($data['icon']),
                    ' ',
                    $value
                ]);
            });
            $grid->column('url', admin_trans('menu.fields.url'))->display(function ($value) {
                if (empty($value) || $value == '#') {
                    return $value;
                }
                return Html::create($value)->tag('a')->redirect($value);
            });
            $grid->column('status', admin_trans('menu.fields.status'))->switch();
            $grid->column('open', admin_trans('menu.fields.open'))->switch();
            $grid->sortInput();
            $grid->quickSearch();
            $grid->setForm()->modal($this->form());
            $grid->updated(function (){
                return message_success(admin_trans('grid.update_success'))->refreshMenu();
            });
            $grid->deleted(function (){
                return message_success(admin_trans('grid.update_success'))->refreshMenu();
            });
        });
    }

    /**
     * 系统菜单
     * @auth true
     * @return Form
     */
    public function form($pid = 0): Form
    {
        return Form::create(new $this->model,function (Form $form) use($pid){
            $form->title(admin_trans('menu.title'));
            $menus = $this->model::select()->toArray();
            array_unshift($menus, ['id' => 0, 'name' => admin_trans('menu.fields.top'), 'pid' => -1]);
            $form->treeSelect('pid', admin_trans('menu.fields.pid'))
                ->default($pid)
                ->options($menus)
                ->required();

            $form->text('name', admin_trans('menu.fields.name'))->required();
            $form->autoComplete('url', admin_trans('menu.fields.url'))
                ->groupOptions(Arr::tree(Admin::node()->all()),'children','title','url')
                ->filterOption();
            $form->icon('icon', admin_trans('menu.fields.icon'))
                ->default('far fa-circle')
                ->required();
            $form->number('sort', admin_trans('menu.fields.sort'))
                ->default($this->model::where('pid', $pid)->max('sort') + 1);
            $form->saved(function(){
                return message_success(admin_trans('form.save_success'))->refreshMenu();
            });
        });

    }
}
