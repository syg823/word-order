<?php
return [
    'add' => 'Add',
    'title' => 'System menu management',
    'fields' => [
        'top' => 'Top menu',
        'pid' => 'Superior menu',
        'name' => 'name',
        'url' => 'url',
        'icon' => 'icon',
        'sort' => 'sort',
        'status' => 'status',
        'open'=>'Menu open',
        'super_status' => 'Super administrator status',
    ],
    'options' => [
        'admin_visible' => [
            [1 => 'show'],
            [0 => 'hidden']
        ]
    ],
    'titles' => [
        'home' => 'Home',
        'system' => 'System',
        'system_manage' => 'System Manage',
        'config_manage' => 'Config Manage',
        'attachment_manage' => 'Attachment Manage',
        'permissions_manage' => 'Permissions Manage',
        'admin' => 'Admin',
        'role_manage' => 'Role Manage',
        'menu_manage' => 'Menu Manage',
        'plug_manage' => 'Plugin Manage',
        'department_manage' => 'Department Manage',
        'post_manage' => 'Post Manage',
    ]
];
