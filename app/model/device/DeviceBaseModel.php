<?php

namespace app\model\device;

use app\model\BaseModel;
use app\model\customer\WorkPlaceModel;
use think\model\concern\SoftDelete;
use think\model\relation\BelongsTo;

/**
 * 设备基本信息
 */
class DeviceBaseModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'device_base';


    /**
     * @param $val
     * @return string
     */
    public function getNameAttr($val): string
    {
        return $val ?: "-";
    }

    /**
     * @return BelongsTo
     */
    public function deviceClass(): BelongsTo
    {
        return $this->belongsTo(DeviceClassModel::class, 'device_class_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function workPlace(): BelongsTo
    {
        return $this->belongsTo(WorkPlaceModel::class, 'work_place_uuid', 'uuid');
    }


}