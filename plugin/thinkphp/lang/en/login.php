<?php
return [
    'account_not_empty'=>'Login account cannot be empty',
    'password_not_empty'=>'Login password cannot be empty',
    'password_min_number'=>'The password must have at least 5 digits',
    'success'=>'Login successful',
    'logout'=>'Log out successfully',
    'error'=>'Account password error',
    'captcha_error'=>'Verification code error',
];
