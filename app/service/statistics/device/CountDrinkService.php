<?php

namespace app\service\statistics\device;

use app\library\DataReturn;
use app\mongo\statistics\device\CountDeviceDaySkuMongo;
use app\mongo\YoPoint\YoPointBrandTerminalProductJournalMongo;
use app\service\DeviceService;
use app\service\WorkPlaceService;
use think\db\BaseQuery;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Log;

/**
 * 获取设备打饮数据
 */
class CountDrinkService
{
    // <editor-fold desc="统计查询输出">

    /**
     * 设备日sku打印数量,按设备分组
     * @param string|array $deviceUUID
     * @param string|array $date
     * @return DataReturn
     */
    public function daySkuGroupDevice($deviceUUID, $date = ''): DataReturn
    {
        if (is_array($deviceUUID)) {
            $deviceSet = array_unique($deviceUUID);
        } else {
            $deviceSet = [$deviceUUID];
        }
        $mongoCountDeviceDaySku = new CountDeviceDaySkuMongo();
        $date = $date ?: date("Y-m-d");
        $where = [
            ['deviceUUID', 'in', $deviceSet]
        ];
        try {
            $resList = $mongoCountDeviceDaySku
                ->when(is_string($date), function (BaseQuery $query) use ($date) {
                    // 满足条件后执行
                    $query->whereDay('unixtime', $date);
                }, function (BaseQuery $query) use ($date) {
                    // 不满足条件执行
                    list($startTime, $endTime) = $date;
                    $query->whereBetweenTime('unixtime', strtotime($startTime), strtotime($endTime));
                })
                ->where($where)->select();
            //$countList = array_fill_keys($deviceSet, []);
            $countList = [];
            if (!empty($resList)) {
                foreach ($resList as $value) {
                    $countList[$value['deviceUUID']][] = array(
                        'drink_sku_name' => $value['drink_sku_name'],
                        'sum' => $value['sum']
                    );
                }
            }
            return dataReturn($countList);
        } catch (DataNotFoundException|ModelNotFoundException|DbException $e) {
            return dataErrorReturn($e->getMessage());
        }
    }

    /**
     * 设备日sku打印数量,按SKU分组
     * @param string|array $deviceUUID
     * @param string|array $date Y-m-d | [Y-m-d, Y-m-d]
     * @return DataReturn
     */
    public function daySkuGroupDrinkName($deviceUUID, $date = ''): DataReturn
    {
        if (is_array($deviceUUID)) {
            $deviceSet = array_unique($deviceUUID);
        } else {
            $deviceSet = [$deviceUUID];
        }

        try {
            $date = $date ?: date("Y-m-d");
            $where = [
                ['deviceUUID', 'in', $deviceSet]
            ];
            $handle = CountDeviceDaySkuMongo::where($where);
            $handle->when(is_string($date), function (BaseQuery $query) use ($date) {
                // 满足条件后执行
                $query->whereDay('unixtime', $date);
            }, function (BaseQuery $query) use ($date) {
                // 不满足条件执行
                list($startTime, $endTime) = $date;
                $query->whereBetweenTime('unixtime', strtotime($startTime), strtotime($endTime));
            });

            $resList = $handle->multiAggregate(['sum' => 'sum'], ['drink_sku_name']);
            $countList = [];
            if (!empty($resList)) {
                foreach ($resList as $value) {
                    $countList[$value['drink_sku_name']] = array(
                        'drink_sku_name' => $value['drink_sku_name'],
                        'sum' => $value['sum_sum']
                    );
                }
            }
            return dataReturn($countList);
        } catch (DataNotFoundException|ModelNotFoundException|DbException $e) {
            return dataErrorReturn($e->getMessage());
        }
    }

    /**
     * 设备日打印总数量
     * @param string|array $deviceUUID
     * @param string|array $date Y-m-d | [Y-m-d, Y-m-d]
     * @return DataReturn
     */
    public function dayTotalGroupDevice($deviceUUID, $date = ''): DataReturn
    {
        if (is_array($deviceUUID)) {
            $deviceSet = array_unique($deviceUUID);
        } else {
            $deviceSet = [$deviceUUID];
        }
        try {
            $date = $date ?: date("Y-m-d");
            $where = [
                ['deviceUUID', 'in', $deviceSet]
            ];

            $handle = CountDeviceDaySkuMongo::where($where);
            $handle->when(is_string($date), function (BaseQuery $query) use ($date) {
                // 满足条件后执行
                $query->whereDay('unixtime', $date);
            }, function (BaseQuery $query) use ($date) {
                // 不满足条件执行
                list($startTime, $endTime) = $date;
                $query->whereBetweenTime('unixtime', strtotime($startTime), strtotime($endTime));
            });
            $resList = $handle->multiAggregate(['sum' => 'sum'], ['deviceUUID']);
            $countList = array_fill_keys($deviceSet, 0);
            if (!empty($resList)) {
                foreach ($resList as $value) {
                    $countList[$value['deviceUUID']] = $value['sum_sum'];
                }
            }
            return dataReturn($countList);
        } catch (DataNotFoundException|ModelNotFoundException|DbException $e) {
            return dataErrorReturn($e->getMessage());
        }

    }

    // </editor-fold>

    // <editor-fold desc="客户打饮统计输出">

    /**
     * @param numeric $customerBaseId
     * @param string|array $date Y-m-d | [Y-m-d, Y-m-d]
     * @return int
     */
    public function customerDeviceTotalDrink($customerBaseId = 0, $date = ''): int
    {
        $count = 0;
        // 先获取设备列表
        $serviceDevice = new DeviceService();
        $deviceList = $serviceDevice->getDeviceList($customerBaseId)->data;
        if (!empty($deviceList)) return $count;
        $deviceUUIDList = array_column($deviceList, 'uuid');
        $deviceDayTotal = $this->dayTotalGroupDevice($deviceUUIDList, $date)->data;
        $count = array_sum($deviceDayTotal);
        return (int)$count;
    }

    // </editor-fold>

    // <editor-fold desc="统计入MongoDB">

    /**
     * 计算某天设备的饮品打印数量(从原始打饮记录Mongob表)
     * @param string $date 基准日期，格式为"Y-m-d"
     * @return mixed
     */
    protected function countDay(string $date = '')
    {
        // 友朋打饮数据 @todo 当前只有友朋的数据
        if (!empty($date)) {
            $handle = YoPointBrandTerminalProductJournalMongo::whereDay('CreateAt', $date);
        } else {
            $handle = YoPointBrandTerminalProductJournalMongo::whereDay('CreateAt');
        }

        $aggList = $handle->where('ReceiptType', '=', '订单出货')->multiAggregate(['sum' => 'Qty'], ['deviceUUID', 'ProductName']);
        if (!empty($aggList)) {
            foreach ($aggList as &$value) {
                $value['sum'] = $value['Qty_sum'];
                unset($value['Qty_sum']);
                $value['drink_sku_name'] = $value['ProductName'];
                unset($value['ProductName']);
                $value['date'] = $date ?: date('Y-m-d');
                $value['unixtime'] = strtotime($value['date']);
            }
        }
        return $aggList;
    }

    /**
     * 同步今日数据
     * @return void
     */
    public function syncToday()
    {
        $day = date("Y-m-d");
        $res = $this->countDay($day);
        $mongoCountDeviceDaySku = new CountDeviceDaySkuMongo();
        if (!empty($res)) {
            foreach ($res as $data) {
                $uniqueWhere = [
                    'deviceUUID' => $data['deviceUUID'],
                    'drink_sku_name' => $data['drink_sku_name'],
                    'date' => $data['date'],
                    'unixtime' => $data['unixtime'],
                ];
                $isEmpty = $mongoCountDeviceDaySku->where($uniqueWhere)->findOrEmpty()->isEmpty();
                if ($isEmpty) {
                    //$log = "Mongo不存在数据,进行保存" . PHP_EOL;
                    $mongoCountDeviceDaySku->insertOne($data);
                } else {
                    //$log = "Mongo存在数据, 更新数据" . PHP_EOL;
                    $mongoCountDeviceDaySku->updateByWehere($data, $uniqueWhere);
                }
                //Log::write($log);
            }
        }
    }

    /**
     * 同步历史天数据
     * @param int $xdays 向前同步几天的数据
     * @return void
     */
    public function syncHistoryDay(int $xdays)
    {
        $dateArr = getPreviousDates(date('Y-m-d'), $xdays);
        $mongoCountDeviceDaySku = new CountDeviceDaySkuMongo();
        foreach ($dateArr as $day) {
            $res = $this->countDay($day);
            if (!empty($res)) {
                foreach ($res as $data) {
                    $uniqueWhere = [
                        'deviceUUID' => $data['deviceUUID'],
                        'drink_sku_name' => $data['drink_sku_name'],
                        'date' => $data['date'],
                        'unixtime' => $data['unixtime'],
                    ];
                    $isEmpty = $mongoCountDeviceDaySku->where($uniqueWhere)->findOrEmpty()->isEmpty();
                    if ($isEmpty) {
                        //$log =  "Mongo不存在数据,进行保存" . PHP_EOL;
                        $mongoCountDeviceDaySku->insertOne($data);
                    } else {
                        //$log =  "Mongo存在数据, 更新数据" . PHP_EOL;
                        $mongoCountDeviceDaySku->updateByWehere($data, $uniqueWhere);
                    }
                    //Log::write($log);
                }
            }
        }
    }


    public function countMonth()
    {

    }

    public function countYear()
    {

    }

    // </editor-fold>


}