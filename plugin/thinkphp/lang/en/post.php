<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-15
 * Time: 23:39
 */
return [
    'title' => 'post',
    'normal' => 'normal',
    'disable' => 'disable',
    'fields' => [
        'name' => 'name',
        'status' => 'status',
        'sort' => 'sort',
        'create_at' => 'Creation time',
    ],
];
