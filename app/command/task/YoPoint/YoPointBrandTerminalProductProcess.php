<?php

namespace app\command\task\YoPoint;

use app\library\DataCollect\YoPointBrand\TerminalProductJournalStruct;
use app\mongo\YoPoint\YoPointBrandTerminalProductJournalMongo;
use app\service\DataCollect\YoPoint\YoPointBrandService;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use think\facade\Log;

/**
 * 友朋品牌商设备流水同步子进程处理
 */
class YoPointBrandTerminalProductProcess extends Command
{
    private YoPointBrandService $yoPointService;

    protected function configure()
    {
        // 指令配置
        $this->setName('友朋品牌商设备流水同步')
            ->addArgument('deviceUUID', Argument::REQUIRED, '设备编号')
            ->addArgument('start_date', Argument::REQUIRED, '开始日期')
            ->addArgument('end_date', Argument::REQUIRED, '结束日期')
            ->setDescription('友朋品牌商设备流水同步子进程处理');
        $this->yoPointService = new YoPointBrandService();
    }

    /**
     * @throws Exception
     */
    protected function execute(Input $input, Output $output)
    {
        if (!$input->hasArgument('deviceUUID')) {
            makeException("deviceUUID Error");
        }
        if (!$input->hasArgument('start_date')) {
            makeException("start_date Error");
        }
        if (!$input->hasArgument('end_date')) {
            makeException("end_date Error");
        }
        $deviceUUID = $input->getArgument('deviceUUID');
        $startDate = $input->getArgument('start_date');
        $endDate = $input->getArgument('end_date');

        $dateRange = "$startDate - $endDate";
        Log::write("YoPointBrandTerminalProductProcess: deviceUUID-$deviceUUID $dateRange");
        $firstPage = 1;
        $pageSize = 100;
        $terminalProductJournalListRes = $this->yoPointService->terminalProductJournalList($deviceUUID, $firstPage, $pageSize, $dateRange);
        if ($terminalProductJournalListRes->code !== 0) {
            makeException($terminalProductJournalListRes->msg);
        }
        $dataList = $terminalProductJournalListRes->data['data'];
        $lastPage = $terminalProductJournalListRes->data['last_page'];
        $currentPage = $terminalProductJournalListRes->data['current_page'];
        for ($i = $currentPage + 1; $i <= $lastPage; $i++) {
            $terminalProductJournalListRes = $this->yoPointService->terminalProductJournalList($deviceUUID, $i, $pageSize, $dateRange);
            if ($terminalProductJournalListRes->code !== 0) {
                makeException($terminalProductJournalListRes->msg);
            }
            $dataList = array_merge($dataList, $terminalProductJournalListRes->data['data']);
        }

        if (!empty($dataList)) {
            $mongo = new YoPointBrandTerminalProductJournalMongo();
            foreach ($dataList as $terminalProductJournal) {
                /**
                 * @var TerminalProductJournalStruct $terminalProductJournal
                 */
                $terminalProductJournal->deviceUUID = $deviceUUID;
                $data = $terminalProductJournal->toArray();
                // mongo 处理
                $isEmpty = $mongo->where(['id' => $terminalProductJournal->id])->findOrEmpty()->isEmpty();
                if ($isEmpty) {
                    echo "Mongo不存在数据,进行保存" . PHP_EOL;
                    $mongo->insertOne($data);
                } else {
                    echo "Mongo存在数据, 更新数据" . PHP_EOL;
                    $mongo->updateByWehere($data, ['id' => $terminalProductJournal->id]);
                }
            }
        }
    }
}