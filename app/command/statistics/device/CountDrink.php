<?php

namespace app\command\statistics\device;

use app\service\statistics\device\CountDrinkService;
use think\console\Command;
use think\console\Input;
use think\console\Output;

/**
 * 同步设备打饮历史数量数量
 */
class CountDrink extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('同步设备打饮历史数量数量')
            ->setDescription('同步设备打饮历史数量数量');
    }

    protected function execute(Input $input, Output $output)
    {
        $service = new CountDrinkService();
        $service->syncHistoryDay(1000);
    }

}