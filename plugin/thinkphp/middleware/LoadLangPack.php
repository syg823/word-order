<?php

namespace plugin\thinkphp\middleware;


use ExAdmin\ui\support\Container;
use think\facade\Cookie;
use think\helper\Arr;
use think\Request;


class LoadLangPack
{

    public function handle(Request $request, \Closure $next)
    {
        $lang = plugin()->thinkphp->config('ui.lang');
        Arr::set($lang,'default',Cookie::get('ex_admin_lang',$lang['default']));
        admin_config(['lang'=>$lang], 'ui');
        app()->lang->switchLangSet($lang['default']);
        Container::getInstance()->translator->setLocale($lang['default']);
        Container::getInstance()->translator->load(plugin()->thinkphp->getPath() . DIRECTORY_SEPARATOR . 'lang', 'ex_admin_ui');
        return $next($request);
    }
}
