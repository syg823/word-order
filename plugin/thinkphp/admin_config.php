<?php

return [
    'token' => [
        'driver'=>\plugin\thinkphp\token\driver\Cache::class,
        //密钥
        'key' => 'QoYEClMJsgOSWUBkSCq26yWkApqSuH3',
        //token有效时长
        'expire' => 86400*365,
        //唯一登录
        'unique' => true,
        //验证字段
        'auth_field' => ['password'],

        'model' => plugin\thinkphp\model\AdminUser::class,
    ],
    //超级管理员id
    'admin_auth_id' => 1,

    'request_interface' => [
        //ExAdmin\ui\contract\LoginAbstract
        'login' => plugin\thinkphp\common\Login::class,
        //ExAdmin\ui\contract\SystemAbstract
        'system' => plugin\thinkphp\common\System::class,
    ],
    'grid' => [
        //ExAdmin\ui\Manager
        'manager' => plugin\thinkphp\grid\GridManager::class,
    ],
    'form' => [
        //ExAdmin\ui\Manager
        'manager' => plugin\thinkphp\form\FormManager::class,
        //ExAdmin\ui\contract\ValidatorAbstract
        'validator' => plugin\thinkphp\form\Validator::class,
        //ExAdmin\ui\contract\UploaderAbstract
        'uploader' => plugin\thinkphp\form\Uploader::class,
    ],
    'echart' => [
        //ExAdmin\ui\Manager
        'manager' => \plugin\thinkphp\echart\EchartManager::class,
    ],
    'route' => [
        //绑定域名
        'domain' => env('DOMAIN.ADMIN_ROUTE_DOMAIN', 'admin'),
        //路由前缀
        'prefix' => env('DOMAIN.ADMIN_ROUTE_PREFIX', ''),
        //中间件
        'middleware' => ['admin'],
    ],
    //菜单
    'menu' => \plugin\thinkphp\service\Menu::class,

    //上传配置
    'upload' => [
        //config/filesystem.php
        'disk' => 'public',
        //保存目录
        'directory' => [
            'image' => 'images',
            'file' => 'files',
        ],
        //禁止上传后缀
        'disabled_ext' => ['php']
    ],
    //扫描权限目录
    'auth_scan' => [
        __DIR__ . '/controller',
        app_path('admin/controller'),
    ],
    'database' => [
        //用户表
        'user_table' => env('database.prefix', '') . 'admin_users',
        'user_model' => plugin\thinkphp\model\AdminUser::class,
        //菜单表
        'menu_table' => env('database.prefix', '') . 'admin_menus',
        'menu_model' => plugin\thinkphp\model\AdminMenu::class,
        //角色表
        'role_table' => env('database.prefix', '') . 'admin_roles',
        'role_model' => plugin\thinkphp\model\AdminRole::class,
        //角色权限关联表
        'role_permission_table' => env('database.prefix', '') . 'admin_role_permissions',
        'role_permission_model' => plugin\thinkphp\model\AdminRolePermission::class,
        //角色菜单关联表
        'role_menu_table' => env('database.prefix', '') . 'admin_role_menus',
        'role_menu_model' => plugin\thinkphp\model\AdminRoleMenu::class,
        //角色用户关联表
        'role_user_table' => env('database.prefix', '') . 'admin_role_users',
        'role_user_model' => plugin\thinkphp\model\AdminRoleUsers::class,
        //系统配置表
        'config_table' => env('database.prefix', '') . 'admin_configs',
        'config_model' => plugin\thinkphp\model\AdminConfig::class,
        //系统附件分类表
        'attachment_cate_table' => env('database.prefix', '') . 'admin_file_attachment_cates',
        'attachment_cate_model' => plugin\thinkphp\model\AdminFileAttachmentCate::class,
        //系统附件表
        'attachment_table' => env('database.prefix', '') . 'admin_file_attachments',
        'attachment_model' => plugin\thinkphp\model\AdminFileAttachment::class,
        //部门表
        'department_table' => 'admin_department',
        'department_model' =>plugin\thinkphp\model\AdminDepartment::class,
        //岗位表
        'post_table' => 'admin_post',
        'post_model' =>plugin\thinkphp\model\AdminPost::class,
        //角色数据权限部门关联表
        'role_department_table' => 'admin_role_department',
        'role_department_model' =>plugin\thinkphp\model\AdminRoleDepartment::class,
    ],
    'cache'=>[
        //缓存目录
        'directory'=>runtime_path()
    ],
    //后台前端UI配置
    'ui' => [
        //语言
        'lang' => [
            // 默认语言
            'default' => config('lang.default', 'zh-CN'),
            //语言列表
            'list' => [
                'zh-CN' => '中文',
                'en' => 'English',
            ]
        ],
        //布局 default classic
        'layout'=>'default',
        //布局方向 horizontal vertical
        'layout_direction'=>'horizontal',
        //主题 light 暗黑dark
        'theme' => 'light',
        //主题色
        'theme_color' => '#1890ff',
        //菜单主题 dark light
        'menu_theme' => 'light',
        //导航模式 sideTopMenuLayout sideMenuLayout topMenuLayout
        'navigationMode' => 'sideTopMenuLayout',
        //布局背景色
        'layout_background'=>'#f0f2f5',
        //顶部栏
        'header'=>[
            //文字颜色
            'text_color'=>'#393939',
            //选中色
            'color'=>'#1890ff',
            //背景色
            'background'=>'#ffffff',
        ],
        //侧边栏
        'sidebar' => [
            //文字颜色
            'text_color'=>'#393939',
            //选中色
            'color'=>'#1890ff',
            //背景色
            'background'=>'#ffffff',
            //宽度
            'width'=>200,
            //是否收起状态
            'collapsed' => false,
            //显示隐藏
            'visible' => true,
            //菜单并排数量
            'menu_num'=>1
        ],
        //多页标签
        'tabs' => true,
        //登录路由
        'loginRoute' => '/ex-admin/login/index',
        //公用渲染路由前缀
        'commonRoutePrefix' => 'common/',
        //后台渲染路由前缀
        'adminRoutePrefix' => '',
    ]
];