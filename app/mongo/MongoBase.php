<?php

namespace app\mongo;

use app\model\BaseModel;

/**
 * Mongo 基类
 */
class MongoBase extends BaseModel
{
    protected $connection = 'mongoDB';
}