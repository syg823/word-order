<?php

namespace plugin\thinkphp\controller;

use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Filter;
use ExAdmin\ui\component\grid\grid\Grid;


/**
 * 岗位管理
 */
class PostController
{
    protected $model;

    public function __construct()
    {
        $this->model = plugin()->thinkphp->config('database.post_model');

    }

    /**
     * 岗位
     * @auth true
     */
    public function index(): Grid
    {
        return Grid::create(new $this->model, function (Grid $grid) {
            $grid->model()->order('sort');
            $grid->title(admin_trans('post.title'));
            $grid->column('name', admin_trans('post.fields.name'));
            $grid->column('status', admin_trans('post.fields.status'))->switch([[1=>''],[0=>'']]);
            $grid->sortInput('sort', admin_trans('post.fields.sort'));
            $grid->column('created_at', admin_trans('post.fields.create_at'));
            $grid->filter(function (Filter $filter) {
                $filter->like()->text('name', admin_trans('post.fields.name'));
                $filter->eq()->select('status', admin_trans('post.fields.status'))->options([
                    1 => admin_trans('post.normal'),
                    0 => admin_trans('post.disable')
                ]);
                $filter->between()->dateRange('created_at', admin_trans('post.fields.create_at'));
            });
            $grid->setForm()->modal($this->form());
        });
    }

    /**
     * 岗位
     * @auth true
     */
    public function form(): Form
    {
        return Form::create(new $this->model, function (Form $form) {
            $form->title(admin_trans('post.title'));
            $form->text('name', admin_trans('post.fields.name'))
                ->required();
            $form->number('sort', admin_trans('post.fields.sort'))->default(0);
        });
    }
}
