<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

class TerminalProductJournalStruct extends StructFactory
{
    public string $id;
    public string $OID;

    public string $deviceUUID;
    public string $ReceiptNo;
    public string $ReceiptType;
    public string $CabinetName;
    public int $CargoNum;
    public string $ProductName;
    public string $BarCode;
    public int $Qty;
    public int $AfterQty;
    public int $CreateAt;
}