<?php


namespace plugin\thinkphp\traits;


use think\model\concern\SoftDelete;

trait SoftDeleteHelp
{
    /**
     * 是否有回收站
     * @param mixed $class
     * @return bool
     */
    public function isTrashed($class): bool
    {
        return in_array(SoftDelete::class, class_uses_recursive($class));
    }
    protected function useSoftDelete($class,$query){
        $reflectionClass = new \ReflectionClass($class);
        $method = $reflectionClass->getMethod('getDeleteTimeField');
        $method->setAccessible(true);
        $field = $method->invoke($class,false);

        $method = $reflectionClass->getMethod('autoWriteTimestamp');
        $method->setAccessible(true);
        $value = $method->invoke($class,$field);

        $query->useSoftDelete($field,$value);
    }
}