<?php

namespace app\admin\controller;

use app\admin\controller\AdminBaseController;
use app\model\device\DeviceTypeModel;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Actions;
use ExAdmin\ui\component\grid\grid\Grid;
use think\Model;

/**
 * 设备类型管理
 */
class DeviceTypeController extends AdminBaseController
{

    /**
     * @inheritDoc
     */
    protected function modelSet(): Model
    {
        return new DeviceTypeModel();
    }

    public function index(): Grid
    {
        // 列表回调函数
        $callFunction = function (Grid $grid) {
            $this->hideGridItems($grid);
            $grid->model()->order(["id" => 'desc']);
            $grid->title("设备类型");
            $grid->column('name', '名称');
            $grid->column('create_time', '创建时间');
            $grid->setForm()->drawer($this->form())->width("50%");

            $grid->actions(function (Actions $action, $data) {
                $action->hideDel();
            });

        };
        return Grid::create($this->model, $callFunction);
    }

    public function form(): Form
    {
        $callFunction = function (Form $form) {
            // 第一行
            $form->row(function (Form $form) {
                // 第一列
                $form->column(function (Form $form) {
                    $form->text("name", "类型名称")
                        ->help('设备类型(咖啡机, 制冰机, 售货机等)')
                        ->required();
                    if (!$form->isEdit()) {
                        $form->hidden("uuid")->value(uuidByComb());
                    }
                })->span(12);

            });
        };
        return Form::create($this->model, $callFunction);
    }

}