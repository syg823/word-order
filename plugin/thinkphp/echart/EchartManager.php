<?php
namespace plugin\thinkphp\echart;



use plugin\thinkphp\echart\Driver\Eloquent;
use think\Model;

class EchartManager extends \ExAdmin\ui\manager\EchartManager
{
    public function setDriver($repository,$component)
    {
        parent::setDriver($repository,$component);
        if($repository instanceof Model){
            $this->driver = new Eloquent();
        }
    }
}
