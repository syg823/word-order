<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-15
 * Time: 23:39
 */
return [
    'title' => 'department',
    'normal' => 'normal',
    'disable' => 'disable',
    'parent_id_repeat'=>'Superior department cannot be this department',
    'fields' => [
        'pid' => 'parent',
        'name' => 'name',
        'leader' => 'leader',
        'mobile' => 'mobile',
        'status' => 'status',
        'sort' => 'sort',
        'create_at' => 'Creation time',
    ],
];
