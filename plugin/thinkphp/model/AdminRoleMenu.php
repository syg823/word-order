<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-14
 * Time: 20:10
 */

namespace plugin\thinkphp\model;



use think\model\Pivot;

class AdminRoleMenu extends Pivot
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.role_menu_table');
        parent::__construct($data);
    }

}
