<?php

namespace app\library;

/**
 * 数据统一返回
 */
class DataReturn
{
    /**
     * @var int
     */
    var int $code = 0;

    /**
     * @var array|object|null
     */
    var $data;

    /**
     * @var string
     */
    var string $msg;

    /**
     * @param int $code
     * @param $data
     * @param string $msg
     */
    public function __construct(int $code, $data, string $msg)
    {
        $this->code = $code;
        $this->data = $data;
        $this->msg = $msg;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * 调用函数的方式调用一个对象时的回应方法
     * 这里相当于toArray
     * @return array
     * @example $dataReturn = new DataReturn(.....); $dataReturn();
     */
    public function __invoke(): array
    {
        return $this->toArray();
    }


}