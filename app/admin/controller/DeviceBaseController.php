<?php

namespace app\admin\controller;

use app\admin\controller\AdminBaseController;
use app\model\BaseModel;
use app\model\device\DeviceBaseModel;
use app\model\device\DeviceClassModel;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Actions;
use ExAdmin\ui\component\grid\grid\Grid;
use think\Model;

class DeviceBaseController extends AdminBaseController
{

    /**
     * @inheritDoc
     */
    protected function modelSet(): Model
    {
        return new DeviceBaseModel();
    }

    public function index()
    {
        $callFunction = function (Grid $grid) {
            $this->hideGridItems($grid);
            $grid->model()->with(['device_class' => ['device_type', 'device_brand'], 'work_place' => ['customer']])
                ->order(["id" => 'desc']);
            $grid->title("设备");
            $grid->column('name', '名称');
            $grid->column('device_class.device_type.name', '类型');
            $grid->column('device_class.device_brand.name', '品牌');
            $grid->column('device_class.name', '型号');
            $grid->column('work_place.name', '职场');


            //$grid->setForm()->drawer($this->form())->width("50%");;


            // 隐藏删除
            $grid->actions(function (Actions $action, $data) {
                $actionsDropdownObj = $action->dropdown();
                if ( $data['data_source_from'] !== BaseModel::dataSourceFromManual ){
                    $action->hideEdit();
                    $action->hideDel();
                }
                $actionsDropdownObj->prepend('设置型号', 'fas fa-house-user')->style(['width'=>'30%'])
                    ->modal($this->setDeviceClass(), ['id'=>$data['id']]);

                $action->hideDel();
            });
        };
        return Grid::create($this->model, $callFunction);
    }

    public function setDeviceClass()
    {
        $callFunction = function (Form $form) {
            // 第一行
            $form->row(function (Form $form) {
                // 第一列
                $form->column(function (Form $form) {
                    $form->selectTable('device_class_id', '型号')
                        ->grid([DeviceClassController::class, 'index'],
                            ['select_device_class' => 1])
                        ->options(function ($ids) {
                            $modelDeviceClass = new DeviceClassModel();
                            $where = [
                                ['id', 'in', $ids],
                                ['status', '=', BaseModel::statusYes]
                            ];
                            //返回选中项
                            $deviceList = $modelDeviceClass->where($where)->with(['device_type', 'device_brand'])->select()->toArray();
                            $options = [];
                            foreach ($deviceList as $value) {
                                $options[$value['id']] = implode('--', [
                                    $value['name'], $value['device_type']['name'], $value['device_brand']['name']
                                ]);
                            }
                            return $options;
                        })->required();
                });
            });
        };
        return Form::create($this->model, $callFunction);
    }


}