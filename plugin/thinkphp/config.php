<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-10
 * Time: 22:10
 */

//\think\facade\Log::write(var_export(app()->request->domain(), true));
//plugin()->thinkphp->getPath();

$domain = app()->request->domain();
$parsedUrl = parse_url($domain); // 解析URL
$subdomain = '';
// 检查是否成功解析了主机部分
if (!empty($parsedUrl['host'])) {
    $hostParts = explode('.', $parsedUrl['host']); // 根据点号拆分主机名
    $subdomain = $hostParts[0]; // 第一个元素通常是子域名
}
//\think\facade\Log::write(var_export($subdomain, true));

switch ($subdomain) {
    case 'customer':
        $config = include plugin()->thinkphp->getPath() . '/customer_config.php';
        break;
    default:
        $config = include plugin()->thinkphp->getPath() . '/admin_config.php';
}

return $config;