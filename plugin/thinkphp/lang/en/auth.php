<?php
return [
    'title'=>'Access rights management',
    'parent'=>'Parent',
    'field_title_grant'=>'Field permissions (hide selected fields)',
    'field_grant'=>'Field permissions',
    'auth_grant'=>'Function permissions',
    'menu_grant'=>'Menu permissions',
    'data_grant'=>'Data permissions',
    'select_user'=>'Choose a person',
    'select_group'=>'Select organization',
    'select_user_tip'=>'Have permission to view data containing the selected person',
    'select_group_tip'=>'You have permission to view data that contains the selected organization',
    'all'=>'All',
    'father_son_linkage'=>'Parent child linkage',
    'fields'=>[
        'name'=>'name',
        'desc'=>'description',
        'status'=>'status',
        'sort' => 'sort',
        'data_type'=>'Data range',
        'department'=>'department',
    ],
    'options'=>[
        'data_type'=>[
            'full_data_rights'=>'All data permissions',
            'data_permissions_for_this_department'=>'Data authority of this department',
            'this_department_and_the_following_data_permissions'=>'Data authority of the Department and below',
            'personal_data_rights'=>'Personal Data permission',
            'custom_data_permissions'=>'Custom data permissions'
        ]
    ]
];
