<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-15
 * Time: 23:39
 */
return [
    'title' => '岗位管理',
    'normal'=>'正常',
    'disable'=>'禁用',
    'fields' => [
        'name' => '岗位名称',
        'status' => '状态',
        'sort' => '排序',
        'create_at' => '创建时间',
    ],
];
