<?php

namespace app\mongo\YoPoint;

use app\mongo\MongoBase;

class YoPointBrandConsumerOrderMongo extends MongoBase
{
    protected $table = 'YoPointBrandConsumerOrder';
    protected $mapping = [];
}