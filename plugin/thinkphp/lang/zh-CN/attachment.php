<?php
return [
    'title'=>'附件管理',
    'download'=>'下载',
    'cate'=>[
        'fields'=>[
            'name'=>'分类名称',
            'pid'=>'上级分类',
            'permission_type'=>'权限类型',
            'sort'=>'排序',
        ],
        'parent'=>'顶级分类',
        'public'=>'所有人',
        'private'=>'私有',
    ]
];
