<?php

namespace plugin\thinkphp\model;

use think\Model;

class AdminConfig extends Model
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.config_table');
        parent::__construct($data);
    }
}
