<?php
namespace plugin\thinkphp\grid;

use plugin\thinkphp\grid\Driver\Eloquent;
use think\Model;


class GridManager extends \ExAdmin\ui\manager\GridManager
{

    public function setDriver($repository,$component)
    {
        parent::setDriver($repository,$component);
        if($repository instanceof Model){
            $this->driver = new Eloquent();
        }
    }
}
