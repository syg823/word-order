<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

class ProductVOStruct extends StructFactory
{
    public string $Name;
    public string $ImageUrl;
    public string $ImageFixWidthUrl;
}