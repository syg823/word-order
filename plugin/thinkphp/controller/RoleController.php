<?php

namespace plugin\thinkphp\controller;

use ExAdmin\ui\component\common\Icon;
use ExAdmin\ui\component\grid\grid\Filter;
use plugin\thinkphp\Admin;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Actions;
use ExAdmin\ui\component\grid\grid\Grid;



/**
 * 系统角色
 */
class RoleController
{
    public function __construct()
    {
        $this->model = plugin()->thinkphp->config('database.role_model');
    }

    /**
     * 系统角色
     * @auth true
     * @return Grid
     */
    public function index(): Grid
    {
        return Grid::create(new $this->model(), function (Grid $grid) {
            $grid->title(admin_trans('auth.title'));
            $grid->column('name', admin_trans('auth.fields.name'));
            $grid->column('desc', admin_trans('auth.fields.desc'));
            $grid->setForm()->modal($this->form());
            $grid->quickSearch();
            $grid->actions(function (Actions $actions, $data) {
                $dropdown = $actions->dropdown();
                $dropdown->prepend(admin_trans('auth.auth_grant'), 'safety-certificate-filled')
                    ->modal($this->auth($data['id']));
                $dropdown->prepend(admin_trans('auth.menu_grant'), 'appstore-filled')
                    ->modal($this->menu($data['id']));
                $dropdown->prepend(admin_trans('auth.data_grant'), 'fas fa-database')
                    ->modal($this->data($data['id']));
            });
        });

    }
    /**
     * 数据权限
     * @auth true
     * @return Form
     */
    public function data($id){
        return Form::create(new $this->model(), function (Form $form) {
            $form->title(admin_trans('auth.title'));
            $form->desc('name', admin_trans('auth.fields.name'));
            $form->desc('desc', admin_trans('auth.fields.desc'));
            $form->select('data_type',admin_trans('auth.fields.data_type'))
                ->required()
                ->options([
                    0=>admin_trans('auth.options.data_type.full_data_rights'),
                    1=>admin_trans('auth.options.data_type.custom_data_permissions'),
                    2=>admin_trans('auth.options.data_type.this_department_and_the_following_data_permissions'),
                    3=>admin_trans('auth.options.data_type.data_permissions_for_this_department'),
                    4=>admin_trans('auth.options.data_type.personal_data_rights'),

                ])
                ->when(1,function (Form $form){
                    $department = plugin()->thinkphp->config('database.department_model');
                    $options = $department::where('status',1)
                        ->select()->toArray();
                    $tree = $form->tree('department')
                        ->showIcon()
                        ->content(Icon::create('FolderOutlined'), 'groupIcon')
                        ->multiple()
                        ->checkable()
                        ->bindAttr('checkStrictly', $form->getModel() . '.check_strictly')
                        ->options($options);
                    $form->popItem();
                    $form->switch('check_strictly',admin_trans('auth.fields.department'))
                        ->default(false)
                        ->checkedChildren(admin_trans('auth.father_son_linkage'))
                        ->unCheckedChildren(admin_trans('auth.father_son_linkage'))
                        ->checkedValue(false)
                        ->unCheckedValue(true)
                        ->getFormItem()->content($tree);

                });
        });
    }
    /**
     * 系统角色
     * @auth true
     * @return Form
     */
    public function form(): Form
    {
        return Form::create(new $this->model(), function (Form $form) {
            $form->title(admin_trans('auth.title'));
            $form->text('name', admin_trans('auth.fields.name'))->required();
            $form->textarea('desc', admin_trans('auth.fields.desc'))->rows(5)->required();
            $form->number('sort', admin_trans('auth.fields.sort'))->default($this->model::max('sort') + 1);
        });

    }

    /**
     * 菜单权限
     * @auth true
     * @return Form
     */
    public function menu($id)
    {
        $menuModel = plugin()->thinkphp->config('database.menu_model');
        $menus = $menuModel::field('id,pid,name')->select()->toArray();
        $roleMenuModel = plugin()->thinkphp->config('database.role_menu_model');
        return $this->commonAuthForm($id, $roleMenuModel, $menus, 'menu_id','name');
    }

    /**
     * 功能权限
     * @auth true
     * @return Form
     */
    public function auth($id)
    {
        $node = Admin::node()->all();
        $rolePermissionModel = plugin()->thinkphp->config('database.role_permission_model');
        return $this->commonAuthForm($id, $rolePermissionModel, $node, 'node_id','title');
    }

    public function commonAuthForm($id, $model, $tree, $field,$label)
    {

        array_unshift($tree, ['id' => 0, $label => admin_trans('auth.all'), 'pid' => -1]);
        $auths = $model::where('role_id', $id)->column($field);
        return Form::create(new $this->model(), function (Form $form) use ($id, $model, $tree, $field, $auths,$label) {
            $form->tree('auth')
                ->options($tree, $label)
                ->default($auths)
                ->checkable();
            $form->saving(function (Form $form) use ($id, $model, $field) {
                $auths = $form->input('auth');
                $form->removeInput('auth');
                $auths = array_filter($auths);
                $auths = array_map(function ($item) use ($id, $field) {
                    return ['role_id' => $id, $field => $item];
                }, $auths);
                $model::where('role_id', $id)->delete();
                $model::insertAll($auths);
            });
        });

    }
}
