<?php

namespace app\mongo\YoPoint;

use app\mongo\MongoBase;

/**
 * 有朋品牌商下设备列表
 */
class YoPointBrandTerminalMongo extends MongoBase
{
    protected $table = 'YoPointBrandTerminal';
    protected $mapping = [];
}