<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateAdminTable extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('admin_users', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统用户表');
        $table->addColumn(Column::string('username', 120)->setUnique()->setDefault('')->setComment('用户账号'));
        $table->addColumn(Column::string('password', 80)->setDefault('')->setComment('密码'));
        $table->addColumn(Column::string('nickname', 50)->setDefault('')->setComment('姓名'));
        $table->addColumn(Column::string('avatar')->setDefault('')->setComment('头像'));
        $table->addColumn(Column::string('email')->setDefault('')->setComment('邮箱'));
        $table->addColumn(Column::string('phone',20)->setDefault('')->setComment('手机号'));
        $table->addColumn(Column::boolean('status')->setDefault(1)->setComment('状态(0:禁用,1:启用)'));
        $table->addTimestamps();
        $table->addSoftDelete();
        $table->create();

        $table = $this->table('admin_menus', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统菜单表');
        $table->addColumn(Column::string('name', 100)->setDefault('')->setComment('名称'));
        $table->addColumn(Column::string('icon', 100)->setDefault('')->setComment('图标'));
        $table->addColumn(Column::string('url')->setDefault('')->setComment('链接'));
        $table->addColumn(Column::string('plugin',50)->setDefault('')->setComment('插件名称'));
        $table->addColumn(Column::integer('pid')->setDefault(0)->setComment('父级id'));
        $table->addColumn(Column::integer('sort')->setDefault(0)->setComment('排序'));
        $table->addColumn(Column::boolean('status')->setDefault(1)->setComment('状态(0:禁用,1:启用)'));
        $table->addColumn(Column::boolean('open')->setDefault(1)->setComment('菜单展开(0:收起,1:展开)'));
        $table->addTimestamps();
        $table->create();

        $table = $this->table('admin_roles', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统角色表');
        $table->addColumn(Column::string('name', 50)->setDefault('')->setComment('权限角色名称'));
        $table->addColumn(Column::string('desc')->setDefault('')->setComment('备注说明'));
        $table->addColumn(Column::integer('sort')->setDefault(0)->setComment('排序'));
        $table->addTimestamps();
        $table->create();

        $table = $this->table('admin_role_permissions', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统角色权限表');
        $table->addColumn(Column::string('node_id')->setDefault('')->setComment('节点id'));
        $table->addColumn(Column::integer('role_id')->setDefault(0)->setComment('角色id'));
        $table->addIndex(['node_id','role_id']);
        $table->create();

        $table = $this->table('admin_role_menus', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统角色菜单表');
        $table->addColumn(Column::integer('menu_id')->setDefault(0)->setComment('菜单id'));
        $table->addColumn(Column::integer('role_id')->setDefault(0)->setComment('角色id'));
        $table->addIndex(['menu_id','role_id']);
        $table->create();

        $table = $this->table('admin_role_users', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统角色用户表');
        $table->addColumn(Column::integer('user_id')->setDefault(0)->setComment('用户id'));
        $table->addColumn(Column::integer('role_id')->setDefault(0)->setComment('角色id'));
        $table->addIndex(['user_id','role_id']);
        $table->create();

        $table = $this->table('admin_configs', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统配置表');
        $table->addColumn(Column::string('name',100)->setDefault('')->setComment('配置字段'));
        $table->addColumn(Column::mediumText('value')->setNullable()->setComment('配置值'));
        $table->addTimestamps();
        $table->create();

        $table = $this->table('admin_file_attachment_cates', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统附件分类');
        $table->addColumn(Column::string('name',100)->setComment('分类名称'));
        $table->addColumn(Column::integer('pid')->setDefault(0)->setComment('上级id'));
        $table->addColumn(Column::tinyInteger('permission_type')->setDefault(0)->setComment('0所有人，1仅自己'));
        $table->addColumn(Column::integer('sort')->setDefault(0)->setComment('排序'));
        $table->addColumn(Column::integer('admin_id')->setDefault(0)->setComment('后台用户id'));
        $table->addTimestamps();
        $table->create();

        $table = $this->table('admin_file_attachments', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])->setComment('系统附件');
        $table->addColumn(Column::integer('cate_id')->setDefault(0)->setComment('分类id'));
        $table->addColumn(Column::integer('uploader_id')->setDefault(0)->setComment('上传人id'));
        $table->addColumn(Column::string('type',20)->setComment('image图片 file文件'));
        $table->addColumn(Column::string('file_type',100)->setDefault('')->setComment('文件类型'));
        $table->addColumn(Column::string('name',100)->setDefault('')->setComment('附件名称'));
        $table->addColumn(Column::string('real_name')->setDefault('')->setComment('原始文件名'));
        $table->addColumn(Column::string('path')->setDefault('')->setComment('路径'));
        $table->addColumn(Column::string('url')->setDefault('')->setComment('访问url'));
        $table->addColumn(Column::string('ext',10)->setDefault('')->setComment('文件后缀'));
        $table->addColumn(Column::string('disk',20)->setDefault('')->setComment('disk'));
        $table->addColumn(Column::bigInteger('size')->setDefault(0)->setComment('文件大小'));
        $table->addSoftDelete();
        $table->addTimestamps();
        $table->create();
    }
}
