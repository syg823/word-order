<?php

use think\migration\Seeder;
use think\facade\Db;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(): void
    {
        $create_time = date('Y-m-d H:i:s');
        //创建管理员
        $table = plugin()->thinkphp->config('database.user_table');

        if(Db::table($table)->select()->isEmpty()){
            Db::table($table)->insert([
                'username' => 'admin',
                'nickname' => 'admin',
                'password' => password_hash('admin',PASSWORD_DEFAULT),
                'avatar' => '/exadmin/img/logo.png',
                'department_id'=>1,
            ]);
        }


        //创建配置
        $table = plugin()->thinkphp->config('database.config_table');
        if(Db::table($table)->select()->isEmpty()) {
            Db::table($table)->insertAll([
                [
                    'name' => 'web_name',
                    'value' => 'Ex-Admin',
                ],
                [
                    'name' => 'web_logo',
                    'value' => '/exadmin/img/logo.png',
                ],
                [
                    'name' => 'web_miitbeian',
                    'value' => '',
                ],
                [
                    'name' => 'web_copyright',
                    'value' => '©版权所有 2014-2021',
                ]
            ]);
        }
        //创建部门
        $table = plugin()->thinkphp->config('database.department_table');
        if(Db::table($table)->select()->isEmpty()) {
            Db::table($table)->insertAll([
                [
                    'name' => '超级管理员',
                    'create_time' => $create_time
                ],
            ]);
        }
        //创建菜单
        $table = plugin()->thinkphp->config('database.menu_table');
        if(Db::table($table)->select()->isEmpty()) {
            Db::table(plugin()->thinkphp->config('database.menu_table'))->insertAll([
                [
                    'pid' => 0,
                    'name' => 'system',
                    'icon' => 'SettingFilled',
                    'url' => '',
                    'sort' => 0,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 1,
                    'name' => 'system_manage',
                    'icon' => 'SettingFilled',
                    'url' => '',
                    'sort' => 1,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 1,
                    'name' => 'home',
                    'icon' => 'fas fa-home',
                    'url' => 'ex-admin/plugin-thinkphp-controller-IndexController/index',
                    'sort' => 0,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 2,
                    'name' => 'config_manage',
                    'icon' => 'far fa-circle',
                    'url' => 'ex-admin/plugin-thinkphp-controller-ConfigController/form',
                    'sort' => 2,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 2,
                    'name' => 'attachment_manage',
                    'icon' => 'far fa-circle',
                    'url' => 'ex-admin/plugin-thinkphp-controller-AttachmentController/index',
                    'sort' => 3,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 1,
                    'name' => 'permissions_manage',
                    'icon' => 'fas fa-users',
                    'url' => '',
                    'sort' => 4,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 6,
                    'name' => 'admin',
                    'icon' => 'far fa-circle',
                    'url' => 'ex-admin/plugin-thinkphp-controller-AdminController/index',
                    'sort' => 5,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 6,
                    'name' => 'role_manage',
                    'icon' => 'far fa-circle',
                    'url' => 'ex-admin/plugin-thinkphp-controller-RoleController/index',
                    'sort' => 6,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 6,
                    'name' => 'menu_manage',
                    'icon' => 'far fa-circle',
                    'url' => 'ex-admin/plugin-thinkphp-controller-MenuController/index',
                    'sort' => 7,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 6,
                    'name' => 'department_manage',
                    'icon' => 'far fa-circle',
                    'url' => 'ex-admin/plugin-thinkphp-controller-DepartmentController/index',
                    'sort' => 8,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 6,
                    'name' => 'post_manage',
                    'icon' => 'far fa-circle',
                    'url' => 'ex-admin/plugin-thinkphp-controller-PostController/index',
                    'sort' => 9,
                    'create_time' => $create_time
                ],
                [
                    'pid' => 0,
                    'name' => 'plug_manage',
                    'icon' => 'fas fa-plug',
                    'url' => 'ex-admin/ExAdmin-ui-plugin-Controller/index',
                    'sort' => 10,
                    'create_time' => $create_time
                ],
            ]);
        }
    }
}