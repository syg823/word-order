<?php

namespace app\admin\controller;

use app\admin\controller\AdminBaseController;
use app\model\BaseModel;
use app\model\customer\CustomerBaseModel;
use app\model\customer\WorkPlaceModel;
use app\mongo\YoPoint\YoPointBrandOperatorMongo;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Actions;
use ExAdmin\ui\component\grid\grid\Grid;
use think\Model;

class WorkPlaceController extends AdminBaseController
{

    /**
     * @inheritDoc
     */
    protected function modelSet(): Model
    {
        return new WorkPlaceModel();
    }


    public function index(): Grid{
        // 列表回调函数
        $callFunction = function (Grid $grid) {
            $this->hideGridItems($grid);
            $grid->model()->order(["id" => 'desc']);
            $grid->title("职场");
            $grid->column('name', '名称');
            $grid->column('alias_name', '别名');
            $grid->column('province', '省');
            $grid->column('city', '市');
            $grid->column('district', '区');
            $grid->column('device_num',  '设备数量');
            $grid->column('create_time', '创建时间');

            $grid->deling(function ($ids) {
                // todo 删除逻辑
                return message_error('失败');
            });

            $grid->setForm()->drawer($this->form())->width("50%");
            $grid->actions(function (Actions $actions, $data) {
                $actionsDropdownObj = $actions->dropdown();
                if ( $data['data_source_from'] !== BaseModel::dataSourceFromManual ){
                    $actions->hideEdit();
                    $actions->hideDel();
                }
                $actionsDropdownObj->prepend('绑定客户', 'fas fa-house-user')->modal($this->bindCustomer(), ['id'=>$data['id']]);
            });


        };
        return Grid::create($this->model, $callFunction);
    }


    public function form(): Form
    {
        $callFunction = function (Form $form) {
            // 第一行
            $form->row(function (Form $form) {
                // 第一列
                $form->column(function (Form $form) {
                    $form->textarea("name", "名称")->rows(3)->required();
                    $form->textarea("username", "用户名")->rows(3)->required();
                    if ( !$form->isEdit() ){
                        $form->password("password", "登录密码")
                            ->allowClear(true)
                            ->default(123456)
                            ->help('初始化密码123456,建议密码包含大小写字母、数字、符号')
                            ->required();
                        $form->hidden("uuid")->value(uuidByComb());
                    }
                })->span(12);

            });
        };
        return Form::create($this->model, $callFunction);
    }

    public function bindCustomer()
    {
        $callFunction = function (Form $form){
            $form->desc('name','客户名称');
            $form->selectTable('customer_base_id', '选择客户')
                ->grid([CustomerController::class, 'index'], ['select_work_place_customer' => 1])
                ->options(function ($ids) {
                    $modelCustomerBase = new CustomerBaseModel();
                    $where = [
                        ['id', 'in', $ids]
                    ];
                    //返回选中项
                    $return = $modelCustomerBase->getIdNameKeyValueList($where);
                    return $return->data;
                })->default("无")->value('无');

        };
        return Form::create($this->model, $callFunction);
    }




}