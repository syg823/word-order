<?php

namespace plugin\thinkphp\filesystem\driver;

use League\Flysystem\AdapterInterface;
use Overtrue\Flysystem\Qiniu\QiniuAdapter;
use think\filesystem\Driver;

class Qiniu extends Driver
{
    protected function createAdapter(): AdapterInterface
    {

        return new QiniuAdapter(
            $this->config['access_key'],
            $this->config['secret_key'],
            $this->config['bucket'],
            $this->config['domain']
        );
    }
    public function url(string $path): string
    {
        return $this->filesystem->getAdapter()->getUrl($path);
    }
}