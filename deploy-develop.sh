#!/bin/sh
if [ ! -f ".env" ]; then
    cp .env.develop .env
fi

export COMPOSER_ALLOW_SUPERUSER=1 && composer update
chown -R www:www ./
chmod 755 -R ./
#/www/server/panel/pyenv/bin/supervisorctl restart readpilot_crontab_develop:*
#/www/server/panel/pyenv/bin/supervisorctl restart readpilot_queue_develop:*