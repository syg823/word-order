<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

/**
 * 设备配置信息
 */
class TerminalConfigVOStruct extends StructFactory
{
    /**
     * @var string 地址
     */
    public string $Address;

    /**
     * @var string 场地策略 id
     */
    public string $PlaceID;

    /**
     * @var string 价格策略 ID
     */
    public string $PriceRuleID;

    /**
     * @var string 虚拟货架 ID
     */
    public string $VirtualShelfID;

    /**
     * @var array [ 经度, 纬度]
     */
    public array $Location;

    /**
     * @var PlaceVOStruct|null 场地策略
     */
    public ?PlaceVOStruct $PlaceVO = null;
}