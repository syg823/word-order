<?php

namespace plugin\thinkphp\model;



use think\model\Pivot;

class AdminRolePermission extends Pivot
{

    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.role_permission_table');
        parent::__construct($data);
    }

}
