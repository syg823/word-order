<?php

namespace app\service;

use app\library\DataReturn;
use app\model\customer\WorkPlaceModel;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 职场服务层
 */
class WorkPlaceService
{
    private WorkPlaceModel $model;

    public function __construct()
    {
        $this->model = new WorkPlaceModel();
    }

    /**
     * 获取完整职场列表
     * @param numeric $customerID
     * @return DataReturn
     */
    public function getWorkPlaceAll($customerID = 0): DataReturn
    {
        $modelQuery = $this->model->newQuery();
        // 职场获取
        try {
            $workPlaceList = $modelQuery
                ->with(['device'])
                ->scope(['customer'], $customerID)
                ->order('id desc')->select()->toArray();
            return dataReturn($workPlaceList);
        } catch (DataNotFoundException|ModelNotFoundException|DbException $e) {
            return dataErrorReturn($e->getMessage());
        }
    }

    /**
     * 获取职场ID_NAME列表
     * @param numeric $customerID
     * @return DataReturn
     */
    public function getWorkPlaceIdNameList($customerID = 0): DataReturn
    {
        $where = [];
        if ($customerID > 0) {
            $where['customer_base_id'] = $customerID;
        }
        return $this->model->getIdNameKeyValueList($where);
    }

    /**
     * 获取职场UUID_NAME列表
     * @param numeric $customerID
     * @return DataReturn
     */
    public function getWorkPlaceUUIDNameList($customerID = 0): DataReturn
    {
        $where = [];
        if ($customerID > 0) {
            $where['customer_base_id'] = $customerID;
        }
        return $this->model->getIdNameKeyValueList($where, 'name', 'uuid');
    }


}