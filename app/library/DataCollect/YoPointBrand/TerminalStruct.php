<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

/**
 * 有朋品牌商服务下的设备结构体
 */
class TerminalStruct extends StructFactory
{
    /**
     * @var string 运营商 ID
     */
    public string $OID;

    /**
     * @var string
     */
    public string $BID;

    /**
     * @var string 设备ID
     */
    public string $id;

    /**
     * @var string 设备名称
     */
    public string $Name;

    /**
     * @var string 设备编号
     */
    public string $DeviceCode;

    /**
     * @var int
     */
    public int $CabinetCount;

    /**
     * @var TerminalConfigVOStruct 设备配置信息
     */
    public TerminalConfigVOStruct $TerminalConfigVO;

    /**
     * @var CargoStruct[] 货道信息
     */
    public array $CargoList;

    /**
     * @var string 生产编号
     */
    public string $Code;

    /**
     * @var int 更新时间
     */
    public int $UpdatedAt;

    /**
     * @var int 设备售货状态 0=停止售货 1=正常售货 2=待投放
     */
    public int $SellStatus;

    /**
     * @var int 激活时间
     */
    public int $ActiveAt;

    /**
     * @var bool 是否在线
     */
    public bool $OnlineStatus;
}