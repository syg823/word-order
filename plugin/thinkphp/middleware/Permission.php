<?php

namespace plugin\thinkphp\middleware;

use Closure;
use plugin\thinkphp\Admin;
use think\exception\HttpResponseException;
use think\Request;


class Permission
{
    public function handle(Request $request, Closure $next)
    {
        list($class,$function) = Admin::getDispatch();
        $method = $request->param('_ajax',$request->method());

        if(!Admin::check($class,$function,$method)){
            //无权限
            throw new HttpResponseException(json(['message' => admin_trans('admin.not_access_permission')], 405));
        }
        return $next($request);
    }
}
