<?php

namespace plugin\thinkphp\model;



use think\Model;

class AdminPost extends Model
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.post_table');
        parent::__construct($data);
    }
}
