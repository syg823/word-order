<?php

namespace app\model\drink;

use app\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 饮品单品(售卖SKU)
 */
class DrinkSkuModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'drink_sku';
}