<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-07-19
 * Time: 20:47
 */

namespace plugin\thinkphp\controller;


use app\service\DeviceService;
use app\service\statistics\device\CountDrinkService;
use ExAdmin\ui\component\common\Html;
use ExAdmin\ui\component\common\Icon;
use ExAdmin\ui\component\echart\LineChart;
use ExAdmin\ui\component\echart\MapChart;
use ExAdmin\ui\component\echart\PieChart;
use ExAdmin\ui\component\grid\badge\Badge;
use ExAdmin\ui\component\grid\card\Card;
use ExAdmin\ui\component\grid\grid\Grid;
use ExAdmin\ui\component\grid\statistic\Statistic;
use ExAdmin\ui\component\layout\layout\Layout;
use ExAdmin\ui\component\layout\Row;
use Illuminate\Routing\Controller;

class IndexController
{
    /**
     * 首页
     * @auth true
     */
    public function index()
    {
        $layout = Layout::create();
        $layout->row(function (Row $row) {
            $row->gutter([10, 10]);
            $row->column(
                Card::create([
                    Icon::create('fas fa-coffee')->style(['fontSize' => '45px', 'color' => '#409eff', 'marginRight' => '20px']),
                    Statistic::create()
                        ->title('今日')
                        ->value($this->todayTotalCups())
                ])->bodyStyle(['display' => 'flex', 'align-items' => 'center'])->hoverable()
                , 6);
            $row->column(
                Card::create([
                    Icon::create('fas fa-hdd')->style(['fontSize' => '45px', 'color' => '#ff9800', 'marginRight' => '20px']),
                    Statistic::create()
                        ->title('设备')
                        ->value($this->totalDevice())
                ])->bodyStyle(['display' => 'flex', 'align-items' => 'center'])->hoverable()
                , 6);

//            $row->column(
//                Card::create([
//                    Icon::create('fas fa-money-bill-alt')->style(['fontSize' => '45px', 'color' => '#e91e63', 'marginRight' => '20px']),
//                    Statistic::create()
//                        ->title('销售额')
//                        ->value(mt_rand(1000, 5000))
//                ])->bodyStyle(['display' => 'flex', 'align-items' => 'center'])->hoverable()
//                , 6);
//
//            $row->column(
//                Card::create([
//                    Icon::create('fas fa-window-restore')->style(['fontSize' => '45px', 'color' => '#4caf50', 'marginRight' => '20px']),
//                    Statistic::create()
//                        ->title('订单')
//                        ->value(mt_rand(1000, 5000))
//                ])->bodyStyle(['display' => 'flex', 'align-items' => 'center'])->hoverable()
//                , 6);
            $row->column(Card::create($this->lineChart())->hoverable(), 18);
            $row->column($this->table(), 6);
            $row->column($this->mapChart(), 12);
            $row->column($this->ringChart(), 12);
        });

        return $layout;
    }

    /**
     * 今日总杯数
     * @return int
     */
    public function todayTotalCups():int
    {
        $service = new CountDrinkService();
        return $service->customerDeviceTotalDrink();
    }

    /**
     * 总设备数
     * @return int
     */
    public function totalDevice():int
    {
        $service = new DeviceService();
        return count($service->getDeviceList()->data);
    }


    public function table()
    {
        $data = [
            [
                'id' => 1,
                'name' => '意式咖啡',
            ],
            [
                'id' => 2,
                'name' => '美式咖啡',
            ],
            [
                'id' => 3,
                'name' => '卡布奇诺',
            ],
            [
                'id' => 4,
                'name' => '拿铁玛奇朵',
            ],
            [
                'id' => 5,
                'name' => '平白咖啡',
            ],
        ];
        return Grid::create($data, function (Grid $grid) {
            $grid->table();
            $grid->column('id', '排名')->display(function ($val) {
                static $i = 0;
                $i++;
                $badge = Badge::create()->count($i);
                if ($i == 1) {
                    $badge->numberStyle(['backgroundColor' => '#ff5722']);
                } elseif ($i == 2) {
                    $badge->numberStyle(['backgroundColor' => '#ffc107']);
                } elseif ($i == 3) {
                    $badge->numberStyle(['backgroundColor' => '#4caf50']);
                } elseif ($i == 4) {
                    $badge->numberStyle(['backgroundColor' => '#03a9f4']);
                } else {
                    $badge->numberStyle(['backgroundColor' => '#9e9e9e']);
                }
                return $badge;
            })->width(100);

            $grid->column('name', '名称');
        });
    }

    public function lineChart()
    {
        //模拟数据
        $data = [];
        for ($i = 0; $i < 12; $i++) {
            $data['a'][] = rand(1000, 5000);
            $data['b'][] = rand(1000, 5000);
            $data['c'][] = rand(1000, 5000);
        }
        return LineChart::create()
            ->height('280px')
            ->header(Html::create('年销量分析')->tag('h3'))
            ->title('销量')
            ->xAxis('year')
            ->data('销量', $data['a'])
            ->data('销量1', $data['b'])
            ->data('销量2', $data['c']);
    }

    public function ringChart()
    {
        return Card::create(
            PieChart::create('趋势')
                //隐藏默认的日期筛选
                ->hideDateFilter()
                //环型
                ->ring()
                ->data('销量', rand(100, 200))
                ->data('销量1', rand(50, 100))
                //可传入闭包
                ->data('销量2', function () {
                    return rand(1, 50);
                })
        );
    }

    public function mapChart()
    {
        //模拟数据
        $data = [];
        for ($i = 0; $i < 7; $i++) {
            $data['a'][] = rand(1000, 5000);
            $data['b'][] = rand(1000, 5000);
            $data['c'][] = rand(1000, 5000);
        }
        return Card::create(
            MapChart::create()
                //隐藏默认的日期筛选
                ->hideDateFilter()
                //添加数据
                ->data('广东', 10)
                ->data('北京', 10)
        );
    }
}