<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

class UserVOStruct extends StructFactory
{
    public string $OpenID;
    public int $Platform;
    public string $id;
    public string $MemberID;
    public string $NickName;
    public int $Sex;
    public string $Province;
    public string $City;
    public string $ImageUrl;
}