<?php

namespace app\library\DataCollect\YoPointBrand;

use app\library\StructFactory;

/**
 * 货道信息
 */
class CargoStruct extends StructFactory
{
    /**
     * @var int 容量
     */
    public int $Capacity;

    /**
     * @var string 货道标识
     */
    public string $DisplayName;

    /**
     * @var string 货柜名称
     */
    public string $CabinetName;

    /**
     * @var int 库存
     */
    public int $Stock;

    /**
     * @var bool 可售状态
     */
    public bool $SellStatus;

    /**
     * @var string 商品条码
     */
    public string $BarCode;

    /**
     * @var bool 是否制冷货道
     */
    public bool $ColdStatus;

    /**
     * @var bool 是否制热货道
     */
    public bool $HotStatus;
}