<?php

namespace plugin\thinkphp\model;

use think\Model;
use think\model\concern\SoftDelete;


class AdminFileAttachment extends Model
{
    use SoftDelete;
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.attachment_table');
        parent::__construct($data);
    }
}
