<?php
return [
    'account_not_empty'=>'登录账号不能为空',
    'password_not_empty'=>'登录密码不能为空',
    'password_min_number'=>'密码最少5位数',
    'success'=>'登陆成功',
    'logout'=>'已退出登录',
    'error'=>'账号密码错误',
    'captcha_error'=>'验证码错误',
];
