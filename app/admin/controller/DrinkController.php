<?php

namespace app\admin\controller;

use app\model\drink\DrinkClassModel;
use app\model\drink\DrinkSkuModel;
use ExAdmin\ui\component\form\Form;
use ExAdmin\ui\component\grid\grid\Grid;
use think\Model;

/**
 * 饮品管理
 */
class DrinkController extends AdminBaseController
{
    private DrinkClassModel $modelDrinkClass;
    private DrinkSkuModel $modelDrinkSku;

    /**
     * @inheritDoc
     */
    protected function modelSet(): Model
    {
        $this->modelDrinkClass = new DrinkClassModel();
        $this->modelDrinkSku = new DrinkSkuModel();
        return $this->modelDrinkSku;
    }

    /**
     * 饮品类型列表
     * @return Grid
     */
    public function classList(): Grid
    {

        $callFunction = function (Grid $grid) {
            $this->hideGridItems($grid);
            $grid->model()->order(["id" => 'desc']);
            $grid->title("饮品类型");
            $grid->column('name', '名称');
            $grid->column('create_time', '创建时间');
            $grid->setForm()->drawer($this->classForm())->width("50%");
        };

        return Grid::create($this->modelDrinkClass, $callFunction);
    }

    /**
     * 饮品类型编辑
     * @return Form
     */
    public function classForm(): Form
    {
        $callFunction = function (Form $form) {
            // 第一行
            $form->row(function (Form $form) {
                // 第一列
                $form->column(function (Form $form) {
                    $form->text("name", "类型名称")
                        ->help("聚合名称, 例如:美式, 意式")
                        ->required();
                    if ( !$form->isEdit() ){
                        $form->hidden("uuid")->value(uuidByComb());
                    }
                })->span(12);
            });
        };

        return Form::create($this->modelDrinkClass, $callFunction);
    }

    /**
     * 饮品SKU列表
     * @return Grid
     */
    public function skuList()
    {

        $callFunction = function (Grid $grid) {
            $this->hideGridItems($grid);
            $grid->model()->order(["id" => 'desc']);
            $grid->title("饮品SKU");
            $grid->column('name', '名称');
            $grid->column('create_time', '创建时间');
        };

        return Grid::create($this->modelDrinkSku, $callFunction);
    }


}