<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
use app\command\Crontab;
use app\command\statistics\device\CountDrink;
use app\command\task\YoPoint\YoPointBrandAsync;
use app\command\task\YoPoint\YoPointBrandAsyncDayTerminalProductJournal;
use app\command\task\YoPoint\YoPointBrandAsyncMonthTerminalProductJournal;
use app\command\task\YoPoint\YoPointBrandAsyncOperators;
use app\command\task\YoPoint\YoPointBrandAsyncTerminals;
use app\command\task\YoPoint\YoPointBrandTerminalProductProcess;
use app\command\test\TestAnything;

return [
    // 指令定义
    'commands' => [
        'testanything' => TestAnything::class,
        'yoPointBrandAsync' => YoPointBrandAsync::class,
        'yoPointBrandAsyncOperators' => YoPointBrandAsyncOperators::class,
        'yoPointBrandAsyncTerminals' => YoPointBrandAsyncTerminals::class,
        'yoPointBrandAsyncDayTerminalProductJournal' => YoPointBrandAsyncDayTerminalProductJournal::class,
        'yoPointBrandAsyncMonthTerminalProductJournal' => YoPointBrandAsyncMonthTerminalProductJournal::class,
        'yoPointBrandTerminalProductProcess' => YoPointBrandTerminalProductProcess::class,
        'countDrink' => CountDrink::class,
        'crontab' => Crontab::class,
    ],
];
