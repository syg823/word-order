<?php

namespace app\model\drink;

use app\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 饮品类型(即分组)
 */
class DrinkClassModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'drink_class';
}