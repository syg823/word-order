<?php

namespace app\model\device;

use app\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 设备类型(咖啡机, 制冰机, 售货机等)
 */
class DeviceTypeModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'device_type';
}