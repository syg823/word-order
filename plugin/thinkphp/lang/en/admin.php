<?php
/**
 * Created by PhpStorm.
 * User: rocky
 * Date: 2022-03-15
 * Time: 23:39
 */
return [
    'system_user' => 'System User',
    'not_access_permission' => 'You do not have permission to access this operation',
    'super_admin' => 'Super administrator',
    'super_admin_delete' => 'Super administrator cannot delete!',
    'super_admin_disabled' => 'Super administrator cannot disable it!',
    'user_info' => 'Member info',
    'reset_password' => 'Reset password',
    'old_password' => 'Old password',
    'old_password_error' => 'Old password error',
    'new_password' => 'New password',
    'confim_password' => 'Confirm password',
    'access_rights'=>'Access rights',
    'normal' => 'normal',
    'disable' => 'disable',
    'username_exist' => 'Duplicate user name',
    'phone_exist' => 'Phone number already exists',
    'password_min_number' => 'The password must have at least 5 digits',
    'password_confim_validate' => 'The passwords entered are inconsistent',
    'update_password'=>'Update Password',
    'fields' => [
        'username' => 'username',
        'nickname' => 'nickname',
        'avatar' => 'avatar',
        'password' => 'password',
        'phone' => 'phone',
        'mail' => 'email',
        'status' => 'status',
        'create_at' => 'Creation time',
    ],
];
