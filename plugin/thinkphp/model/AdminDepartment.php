<?php

namespace plugin\thinkphp\model;


use think\Model;

class AdminDepartment extends Model
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.department_table');
        parent::__construct($data);
    }
    protected function getPidAttr($value){
        return (int) $value;
    }
}
