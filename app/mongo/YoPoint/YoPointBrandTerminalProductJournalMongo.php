<?php

namespace app\mongo\YoPoint;

use app\mongo\MongoBase;

/**
 * @method multiAggregate(string[] $array, string[] $array1)
 */
class YoPointBrandTerminalProductJournalMongo extends MongoBase
{
    protected $table = 'YoPointBrandTerminalProductJournal';
    protected $mapping = [];
}