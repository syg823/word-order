<?php
if (!function_exists('admin_sysconf')) {

    /**
     * 配置系统参数
     * @param string $name 参数名称
     * @param string $value 无值为获取
     * @return mixed
     */
    function admin_sysconf($name = null, $value = null)
    {
        $model = plugin()->thinkphp->config('database.config_model');
        if(is_null($name)){
            return $model::select()->toArray();
        }
        if (is_null($value)) {
            $value =$model::where('name', $name)->value('value');
            if (is_null($value)) {
                return $value;
            };
            $json = json_decode($value, true);
            if (is_array($json)) {
                return $json;
            } else {
                return $value;
            }
        } else {
            if (is_array($value)) {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            }
            $result = $model::where('name', $name)->find();
            if ($result) {
                return $model::where('name', $name)->update(['value' => $value]);
            } else {
                return $model::insert([
                    'name' => $name,
                    'value' => $value,
                ]);
            }
        }
    }
}
