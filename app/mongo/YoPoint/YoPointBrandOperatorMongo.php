<?php

namespace app\mongo\YoPoint;

use app\mongo\MongoBase;

/**
 * 有朋品牌商下运营商列表
 */
class YoPointBrandOperatorMongo extends MongoBase
{
    protected $table = 'YoPointBrandOperator';
    protected $mapping = [];

}