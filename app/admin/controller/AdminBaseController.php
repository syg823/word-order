<?php

namespace app\admin\controller;

use app\model\BaseModel;
use ExAdmin\ui\component\grid\grid\Grid;
use think\Model;

// ex-admin/app-admin-controller-XXXController/index
abstract class AdminBaseController
{
    /**
     * @var Model|BaseModel
     */
    protected Model $model;

    public function __construct()
    {
        $this->model = $this->modelSet();
        $this->_init();
    }

    /**
     * 设置模型
     * @return Model
     */
    abstract protected function modelSet(): Model;

    /**
     * 初始化方法
     * @return void
     */
    protected function _init()
    {
    }

    /**
     * 默认Grid隐藏项目
     * @param Grid $grid
     * @return Grid
     */
    protected function hideGridItems(Grid $grid){
        $grid->hideDelete()
            ->hideTrashed()
            ->hideDeleteSelection()
            ->hideSelection();
        return $grid;
    }

}