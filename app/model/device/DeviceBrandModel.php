<?php

namespace app\model\device;

use app\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 设备品牌模型
 */
class DeviceBrandModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'device_brand';
}