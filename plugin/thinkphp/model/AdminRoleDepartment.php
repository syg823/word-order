<?php

namespace plugin\thinkphp\model;

use think\Model;

class AdminRoleDepartment extends Model
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.role_department_table');
        parent::__construct($data);
    }
}
