<?php

namespace plugin\thinkphp\model;

use think\Model;

class AdminFileAttachmentCate extends Model
{
    public function __construct(array $data = [])
    {
        $this->table = plugin()->thinkphp->config('database.attachment_cate_table');
        parent::__construct($data);
    }
}
