<?php

namespace app\model\system;

use app\model\BaseModel;
use think\model\concern\SoftDelete;

class SystemAreaModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'sys_area';

    /**
     * 省级
     */
    const levelProvince = 1;

    /**
     * 市级
     */
    const levelCity = 2;

    /**
     * 区级
     */
    const levelDistrict = 3;

}